#ifndef SAMPLES_H_INCLUDED
#define SAMPLES_H_INCLUDED

void run_functions_sample();
void run_objects_sample();
void run_interfaces_sample();
void run_operators_sample();
void run_errors_sample();

#endif

