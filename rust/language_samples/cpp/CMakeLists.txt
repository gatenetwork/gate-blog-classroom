project(rust_vs_cpp_samples)

file(GLOB HEADERS
  "*.h"
  "*.hpp"
)
file(GLOB SOURCES
  "*.c"
  "*.cpp"
)

add_executable(${PROJECT_NAME}
  ${HEADERS}
  ${SOURCES}
)

