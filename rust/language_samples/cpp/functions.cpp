#include "samples.h"
#include <stdio.h>

static double add(int a, float b)
{ // local function
  const double d = (double)a + (double)b;
  return d;
}

void run_functions_sample()
{ // public extern function
  printf("==== run_functions_sample ====\n");
  const double d = add(12, 34.0f);
  printf("%lf\n", d);
}