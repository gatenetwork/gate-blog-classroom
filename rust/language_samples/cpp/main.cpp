#include "samples.h"

#include <iostream>

int main()
{
  std::cout << "Hello World!" << std::endl;

  run_functions_sample();
  run_objects_sample();
  run_interfaces_sample();
  run_operators_sample();
  run_errors_sample();

  return 0;
}