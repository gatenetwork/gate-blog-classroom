#include "samples.h"
#include <stdio.h>
#include <string>
#include <iostream>

struct Point
{
  float x;
  float y;

  Point(float xx, float yy) 
  : x(xx), y(yy)
  {    
  }
};

using std::to_string;

std::string to_string(Point const& point)
{
  std::string text;
  text += "{";
  text += std::to_string(point.x);
  text += ",";
  text += std::to_string(point.y);
  text += "}";
  return text;
}

Point operator+(Point const& l, Point const& r)
{
  return Point(l.x + r.x, l.y + r.y);
}
Point& operator+=(Point& l, Point const& r)
{
  l.x += r.x;
  l.y += r.y;
  return l;
}

Point operator-(Point const& l, Point const& r)
{
  return Point(l.x - r.x, l.y - r.y);
}
Point& operator-=(Point& l, Point const& r)
{
  l.x -= r.x;
  l.y -= r.y;
  return l;
}

Point operator*(Point const& l, float const& factor)
{
  return Point(l.x * factor, l.y * factor);
}
Point& operator*=(Point& l, float const& factor)
{
  l.x *= factor;
  l.y *= factor;
  return l;
}

Point operator/(Point const& l, float const& denominator)
{
  return Point(l.x / denominator, l.y / denominator);
}
Point& operator/=(Point& l, float const& denominator)
{
  l.x /= denominator;
  l.y /= denominator;
  return l;
}

static void use_point_operators() 
{
  const Point p1(1.0, 2.0);
  const Point p2(2.0, 3.0);
    
  auto p3 = p1 + p2;
  std::cout << to_string(p1) << " + " << to_string(p2) << " = " << to_string(p3) << std::endl;

  auto p4 = p2 - p1;
  std::cout << to_string(p2) << " - " << to_string(p1) << " = " << to_string(p4) << std::endl;

  auto factor = 10.0f;
  auto p5 = p2 * factor;
  std::cout << to_string(p2) << " * " << to_string(factor) << " = " << to_string(p5) << std::endl;

  auto denominator = 5.0f;
  auto p6 = p5 / denominator;
  std::cout << to_string(p5) << " / " << to_string(denominator) << " = " << to_string(p6) << std::endl;
}

void run_operators_sample()
{
  printf("==== run_operators_sample ====\n");
  use_point_operators();
}