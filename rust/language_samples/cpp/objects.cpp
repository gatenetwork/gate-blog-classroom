#include "samples.h"
#include <stdio.h>

class Thing
{
private:
  int m_foo;
  int m_bar;
public:
  Thing(int foo)
  : m_foo(foo),
    m_bar(0)
  {    
  }
  static char const* get_type_description()
  {
    return "This is a Thing type.";
  }
  int get_foo() const
  {
    return this->m_foo;
  }
  int get_bar() const
  {
    return this->m_bar;
  }
  void set_bar(int value)
  {
    m_bar = value;
  }
};

static int use_thing()
{
  const int foo = 12;
  Thing thing(foo);
  thing.set_bar(13);
  printf("Description = %s\n", Thing::get_type_description());
  printf("foo=%d, bar=%d\n", thing.get_foo(), thing.get_bar());
  return 0;
}

void run_objects_sample()
{
  printf("==== run_objects_sample ====\n");
  use_thing();
}