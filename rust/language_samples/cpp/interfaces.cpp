#include "samples.h"

#include <stdio.h>
#include <string>
#include <iostream>

class Animal
{
public:
  virtual ~Animal() {}
  virtual std::string get_name() const = 0;
  virtual void speak() const = 0;
};
class Cat : public Animal
{
private:
  std::string m_name;
public:
  Cat(char const* name)
  : m_name(name)
  {
  }
  std::string get_name() const override
  {
    return m_name;
  }
  void speak() const override
  {
    printf("Meow\n");
  }
};
class Dog : public Animal
{
private:
  std::string m_name;
public:
  Dog(char const* name)
  : m_name(name)
  {
  }
  std::string get_name() const override
  {
    return m_name;
  }
  void speak() const override
  {
    printf("Woof\n");
  }
};
static void talk_with_animal(Animal const& animal) 
{
  std::cout << "Animals's name is: " << animal.get_name() << std::endl;
  animal.speak();
}

static void handle_animals() 
{
  Cat c("Kitty");
  Dog d("Sparky");
  
  talk_with_animal(c);
  talk_with_animal(d);
}

void run_interfaces_sample()
{
  printf("==== run_interfaces_sample ====\n");
  handle_animals();
}