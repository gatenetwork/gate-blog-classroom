#include "samples.h"
#include <stdio.h>
#include <string>
#include <iostream>

struct KeyValue
{
  std::string key;
  std::string value;
};

struct ParserError
{
  std::string error_message;
};

static KeyValue parse_key_value(std::string const& text) throw(ParserError)
{
  size_t pos = text.find_first_of("=");
  if(pos != std::string::npos)
  {
    KeyValue kv;
    kv.key = text.substr(0, pos);
    kv.value = text.substr(pos + 1);
    return kv;
  }
  else
  {
    ParserError error;
    error.error_message = "Could not parse text";
    throw error;
  }
}

static void parse_text(std::string const& text) throw(ParserError)
{
  KeyValue key_value_pair = parse_key_value(text);
  std::cout << "First token: " << key_value_pair.key << std::endl;
  std::cout << "Second token: " << key_value_pair.value << std::endl;
}

static void use_parser(std::string const& text)
{
  std::cout << "Parsing text: " << text << std::endl;
  try 
  {
    parse_text(text);
  }
  catch(ParserError const& parser_error)
  {
    std::cout << "Parsing has failed: " << parser_error.error_message << std::endl;
  }
}

void run_errors_sample() 
{
  printf("==== run_errors_sample ====\n");
  use_parser("correct=key_value_pair");
  printf("----------------------------------------\n");
  use_parser("not_a_key_value_pair");
}