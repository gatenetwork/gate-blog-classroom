
mod functions;
mod objects;
pub mod interfaces;
mod operators;
mod errors;

pub fn main() {
    println!("Hello, world!");

    functions::run_functions_sample();
    objects::run_objects_sample();
    interfaces::run_interfaces_sample();
    operators::run_operators_sample();
    errors::run_errors_sample();

    std::process::exit(0);
}
