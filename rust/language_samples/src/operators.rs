use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Sub;
use std::ops::SubAssign;
use std::ops::Mul;
use std::ops::MulAssign;
use std::ops::Div;
use std::ops::DivAssign;

// struct must derive Copy and Clone, 
// otherwise operator parameters are consumed and cannot be used again
#[derive(Copy, Clone)]
pub struct Point {
  pub x: f32,
  pub y: f32
}

impl Point {
  pub fn new(x: f32, y: f32) -> Self {
    Self { 
      x: x, 
      y: y
    }
  }
}

impl ToString for Point {
  fn to_string(&self) -> String {
    let mut text = String::new();
    let str_x = self.x.to_string();
    let str_y = self.y.to_string();
    text.push_str("{");
    text.push_str(&str_x);
    text.push_str(",");
    text.push_str(&str_y);
    text.push_str("}");
    text
  }
}

impl Add for Point {
  type Output = Point;    
  fn add(self, other: Self) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}
impl AddAssign for Point {
  fn add_assign(&mut self, other: Self) {
    self.x += other.x;
    self.y += other.y;
  }
}
impl Sub for Point {
  type Output = Self;
      
  fn sub(self, other: Self) -> Self::Output {
    Point::new(self.x - other.x, self.y - other.y)
  }
}
impl SubAssign for Point {
  fn sub_assign(&mut self, other: Self) {
    self.x -= other.x;
    self.y -= other.y;
  }
}
impl Mul<f32> for Point {
  type Output = Self;
      
  fn mul(self, factor: f32) -> Self::Output {
    Point::new(self.x * factor, self.y * factor)
  }
}
impl MulAssign<f32> for Point {
  fn mul_assign(&mut self, factor: f32) {
    self.x *= factor;
    self.y *= factor;
  }
}
impl Div<f32> for Point {
  type Output = Self;
      
  fn div(self, denominator: f32) -> Self::Output {
    Point::new(self.x / denominator, self.y / denominator)
  }
}
impl DivAssign<f32> for Point {
  fn div_assign(&mut self, denominator: f32) {
    self.x /= denominator;
    self.y /= denominator;
  }
}
        
fn use_point_operators() {
  let p1 = Point::new(1.0, 2.0);
  let p2 = Point::new(2.0, 3.0);
    
  let p3 = p1 + p2;
  println!("{} + {} = {}", p1.to_string(), p2.to_string(), p3.to_string());

  let p4 = p2 - p1;
  println!("{} - {} = {}", p2.to_string(), p1.to_string(), p4.to_string());

  let factor = 10.0;
  let p5 = p2 * factor;
  println!("{} * {} = {}", p2.to_string(), factor, p5.to_string());

  let denominator = 5.0;
  let p6 = p5 / denominator;
  println!("{} / {} = {}", p5.to_string(), denominator, p6.to_string());
}

pub fn run_operators_sample() {
  println!("==== run_operators_sample ====");

  use_point_operators();
}

#[test]
fn test_point(){
  use_point_operators();
}