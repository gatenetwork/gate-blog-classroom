pub trait Animal {
  fn get_name(&self) -> String;
  fn speak(&self);
}

struct Cat {
  m_name: String
}

impl Cat {
  pub fn new(name: &str) -> Self {
    Self {
      m_name: name.to_string()
    }
  }
}

impl Animal for Cat {
  fn get_name(&self) -> String {
    self.m_name.clone()
  }
  fn speak(&self) {
    println!("Meow");
  }
}

struct Dog {
  m_name: String
}

impl Dog {
  pub fn new(name: &str) -> Self {
    Self {
      m_name: name.to_string()
    }
  }
}

impl Animal for Dog {
  fn get_name(&self) -> String {
    self.m_name.clone()
  }
  fn speak(&self) {
    println!("Woof");
  }
}

fn talk_with_animal(animal: &dyn Animal) {
  println!("Animals's name is: {}", animal.get_name());
  animal.speak();
}

fn handle_animals() {
  let c = Cat::new("Kitty");
  let d = Dog::new("Sparky");

  let a: &dyn Animal = &c;
  talk_with_animal(a);

  let a: &dyn Animal = &d;
  talk_with_animal(a);
}

pub fn run_interfaces_sample() {
  println!("==== run_interface_sample ====");

  handle_animals();
}

#[test]
fn test_handle_animals()
{
  handle_animals();
}
