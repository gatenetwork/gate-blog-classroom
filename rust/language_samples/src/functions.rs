fn add(a: i32, b: f32) -> f64 {
  // local function, cast parameter to other types
  let d = a as f64  + b as f64;
  // last line without ";" is equal to "return d;"
  d
}

pub fn run_functions_sample() {
  println!("==== run_functions_sample ====");

  let d = add(12, 34.0);
  println!("{}", d);
}

#[test]
fn test_run_add()
{
  run_functions_sample();
}
