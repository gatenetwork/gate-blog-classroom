struct Thing {
  m_foo: i32,
  m_bar: i32
}

impl Thing {
  pub fn new(foo: i32) -> Self {
    // return a new constructed Thing
    Self {
      m_foo: foo,
      m_bar: 0
    }
  }
  pub fn get_type_description() -> &'static str {
    // return a static immutable string reference
    "This is a Thing type."
  }

  pub fn get_foo(&self) -> i32 {
    // full return statement
    return self.m_foo;
  }
  pub fn get_bar(&self) -> i32 {
    // short return statement
    self.m_bar
  }
  pub fn set_bar(&mut self, bar: i32) {
    self.m_bar = bar;
  }
}

fn use_thing() {
  let foo = 12;
  let mut thing = Thing::new(foo);
  thing.set_bar(13);
  println!("Description = {}", Thing::get_type_description());
  println!("foo={}, bar={}", thing.get_foo(), thing.get_bar());
}

pub fn run_objects_sample() {
  println!("==== run_objects_sample ====");

  use_thing();
}

#[test]
fn test_use_thing() {
    use_thing();
}
