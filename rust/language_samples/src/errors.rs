
struct KeyValue {
  key: String,
  value: String
}

struct ParserError {
  error_message: String
}


fn parse_for_key_value(text: &str) -> Result<KeyValue, ParserError> {
  let tokens = text.split_once("=");

  match tokens {
    Some((first, second)) => {
      let key_value_pair = KeyValue { 
        key: first.to_string(), 
        value: second.to_string()
      };
      Ok(key_value_pair)
    }

    None => {
      Err(ParserError {
        error_message: "Could not parse text".to_string()
      })
    }
  }
}

fn parse_text(text: &str) -> Result<(), ParserError> {
  let key_value_pair = parse_for_key_value(text)?;
  println!("First token: {}", key_value_pair.key);
  println!("Second token: {}", key_value_pair.value);
  Ok(())
}

fn use_parser(text: &str) {
  println!("Parsing text: {}", text);
  let parser_result = parse_text(text);

  match parser_result {
    Ok(()) => {
        println!("Text successfully parsed");
    }
    Err(p) => {
      println!("Parsing has failed: {}", p.error_message);
    }

  }
}


pub fn run_errors_sample() {
  println!("==== run_errors_sample ====");

  use_parser("correct=key_value_pair");
  println!("----------------------------------------");
  use_parser("not_a_key_value_pair");
}

#[test]
fn test_errors_add()
{
  run_errors_sample();
}
