#include <windows.h>

typedef enum {
  WSL_DISTRIBUTION_FLAGS_NONE = 0x0,
  WSL_DISTRIBUTION_FLAGS_ENABLE_INTEROP = 0x1,
  WSL_DISTRIBUTION_FLAGS_APPEND_NT_PATH = 0x2,
  WSL_DISTRIBUTION_FLAGS_ENABLE_DRIVE_MOUNTING = 0x4
} WSL_DISTRIBUTION_FLAGS;

typedef struct 
{
    HRESULT (*ConfigureDistribution)(PCWSTR distributionName, ULONG defaultUID, WSL_DISTRIBUTION_FLAGS wslDistributionFlags);
    HRESULT (*GetDistributionConfiguration)(PCWSTR distributionName, ULONG* distributionVersion, ULONG* defaultUID, 
                WSL_DISTRIBUTION_FLAGS* wslDistributionFlags, PSTR** defaultEnvironmentVariables, ULONG* defaultEnvironmentVariableCount);
    BOOL(*IsDistributionRegistered)(PCWSTR distributionName);
    HRESULT(*Launch)(PCWSTR distributionName, PCWSTR command, BOOL useCurrentWorkingDirectory, HANDLE stdIn, HANDLE stdOut, HANDLE stdErr, HANDLE *process);
    HRESULT(*LaunchInteractive)(PCWSTR distributionName, PCWSTR command, BOOL useCurrentWorkingDirectory, DWORD* exitCode);
    HRESULT(*RegisterDistribution)(PCWSTR distributionName, PCWSTR tarGzFilename);
    HRESULT(*UnregisterDistribution)(PCWSTR distributionName);
} wsl_api_t;

static wsl_api_t wsl_api;

static int load_func(HMODULE hmod, char const* func_name, void* ptr_funcptr)
{
    FARPROC proc = GetProcAddress(hmod, func_name);
    if(proc)
    {
        *((FARPROC*)ptr_funcptr) = proc;
        return 1;
    }
    else
    {
        *((void**)ptr_funcptr) = NULL;
        return 0;
    }
}

static int load_wsl_api()
{
    HMODULE hmod = LoadLibrary("wslapi");
    FARPROC proc;

    load_func(hmod, "WslConfigureDistribution", wsl_api.ConfigureDistribution);
    load_func(hmod, "WslGetDistributionConfiguration", wsl_api.GetDistributionConfiguration);
    load_func(hmod, "WslIsDistributionRegistered", wsl_api.IsDistributionRegistered);
    load_func(hmod, "WslLaunch", wsl_api.Launch);
    load_func(hmod, "WslLaunchInteractive", wsl_api.LaunchInteractive);
    load_func(hmod, "WslRegisterDistribution", wsl_api.RegisterDistribution);
    load_func(hmod, "WslUnregisterDistribution", wsl_api.UnregisterDistribution);
}


