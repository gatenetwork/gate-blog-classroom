#ifndef CLASSIC_VARIANT_VARIANT_HPP_INCLUDED
#define CLASSIC_VARIANT_VARIANT_HPP_INCLUDED

#include <new>
#include <typeinfo>

namespace
{
	template<unsigned N> struct InvalidVariantArg
	{
	private:
		InvalidVariantArg(InvalidVariantArg const&);
	};

	template<class T> void type_cctor(void* dst, void const* src)
	{
		new(dst) T(*static_cast<T const*>(src));
	}
	template<class T> void type_dtor(void* dst)
	{
		static_cast<T*>(dst)->~T();
	}

}


template<class T0,
	class T1 = InvalidVariantArg<1>,
	class T2 = InvalidVariantArg<2>,
	class T3 = InvalidVariantArg<3>,
	class T4 = InvalidVariantArg<4>,
	class T5 = InvalidVariantArg<5>,
	class T6 = InvalidVariantArg<6>,
	class T7 = InvalidVariantArg<7>,
	class T8 = InvalidVariantArg<8>,
	class T9 = InvalidVariantArg<9>
>
class Variant
{
public:
	typedef Variant<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> self_t;
private:
	typedef void(*cctor_t)(void* dst, void const* src);
	typedef void(*dtor_t)(void* dst);

	size_t type_index;
	std::type_info const* ptr_type;	
	cctor_t	cctor;					// copy constructor dispatcher
	dtor_t	dtor;					// destructor dispatcher

	union
	{
		void*	alignment_dummy1;
		double	alignment_dummy2;
		int		(*alignment_dummy3)(int);
		char	d0[sizeof(T0)];
		char	d1[sizeof(T1)];
		char	d2[sizeof(T2)];
		char	d3[sizeof(T3)];
		char	d4[sizeof(T4)];
		char	d5[sizeof(T5)];
		char	d6[sizeof(T6)];
		char	d7[sizeof(T7)];
		char	d8[sizeof(T8)];
		char	d9[sizeof(T9)];
	} content;

	template<class U> void copy_construct(size_t index, U const& src)
	{
		this->type_index = index;
		this->cctor = &type_cctor<U>;
		this->dtor = &type_dtor<U>;
		this->ptr_type = &typeid(U);
		this->cctor(&this->content, &src);
	}
	void destruct()
	{
		this->dtor(&this->content);
	}
	template<class U> self_t& assign(size_t index, U const& src)
	{
		this->destruct();
		this->copy_construct(index, src);
		return *this;
	}

public:

	Variant() { this->construct(0, T0()); }
	Variant(T0 const& t) { this->copy_construct(0, t); }
	Variant(T1 const& t) { this->copy_construct(1, t); }
	Variant(T2 const& t) { this->copy_construct(2, t); }
	Variant(T3 const& t) { this->copy_construct(3, t); }
	Variant(T4 const& t) { this->copy_construct(4, t); }
	Variant(T5 const& t) { this->copy_construct(5, t); }
	Variant(T6 const& t) { this->copy_construct(6, t); }
	Variant(T7 const& t) { this->copy_construct(7, t); }
	Variant(T8 const& t) { this->copy_construct(8, t); }
	Variant(T9 const& t) { this->copy_construct(9, t); }

	Variant(self_t const& that)
		: type_index(that.type_index),
		ptr_type(that.ptr_type),
		cctor(that.cctor),
		dtor(that.dtor)
	{
		this->cctor(&this->content, &that.content);
	}

	self_t& operator=(self_t const& that)
	{
		if(this != &that)
		{
			this->destruct();
			this->type_index = that.type_index;
			this->ptr_type = that.ptr_type;
			this->cctor = that.cctor;
			this->dtor = that.dtor;
			this->cctor(&this->content, &that.content);
		}
		return *this;
	}

	self_t& operator=(T0 const& t) { return this->assign(0, t); }
	self_t& operator=(T1 const& t) { return this->assign(1, t); }
	self_t& operator=(T2 const& t) { return this->assign(2, t); }
	self_t& operator=(T3 const& t) { return this->assign(3, t); }
	self_t& operator=(T4 const& t) { return this->assign(4, t); }
	self_t& operator=(T5 const& t) { return this->assign(5, t); }
	self_t& operator=(T6 const& t) { return this->assign(6, t); }
	self_t& operator=(T7 const& t) { return this->assign(7, t); }
	self_t& operator=(T8 const& t) { return this->assign(8, t); }
	self_t& operator=(T9 const& t) { return this->assign(9, t); }

	~Variant()
	{
		destruct();
	}

	std::type_info const& type_info() const
	{
		return *this->ptr_type;
	}

	template<class U> bool is_a() const
	{
		return this->type_info() == typeid(U);
	}


	template<class U> U* get_ptr()
	{
		if(this->is_a<U>())
		{
			return NULL;

		}
		return reinterpret_cast<U*>(&this->content);
	}
	template<class U> U const* get_ptr() const
	{
		if(!this->is_a<U>())
		{
			return NULL;

		}
		return reinterpret_cast<U const*>(&this->content);
	}

	size_t index() const
	{
		return this->type_index;
	}

	template<class U> U& get()
	{
		if(!this->is_a<U>())
		{
			throw 1;
		}
		return *reinterpret_cast<U*>(&this->content);
	}

	template<class U> U const& get() const
	{
		if(!this->is_a<U>())
		{
			throw 1;
		}
		return *reinterpret_cast<U const*>(&this->content);
	}
};



#endif 