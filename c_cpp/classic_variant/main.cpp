#include <string>
#include <iostream>
#include <variant.hpp>


int main()
{
	Variant<int, std::string> v(12);
	std::cout << v.get<int>() << std::endl;

	v = std::string("Hello World");
	std::cout << v.get<std::string>() << std::endl;

	v = 1234567;
	std::cout << v.type_info().name() << std::endl;

	v = "abc";
	std::cout << v.type_info().name() << std::endl;
	return 0;
}

