
add_executable(multiple_shared_one_static_libs
    main.c
)

if(UNIX)
  target_link_libraries(multiple_shared_one_static_libs
    dl
  )
endif()
