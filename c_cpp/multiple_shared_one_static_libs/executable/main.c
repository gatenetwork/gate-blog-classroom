#include <stdio.h>


static int load_function_from_shared_lib(char const* lib_name, char const* func_name, void** ptr_function);

#if defined(WIN32)

#define LIB_FILE_PREFIX ""
#define LIB_FILE_SUFFIX ".dll"

#include <windows.h>

static int load_function_from_shared_lib(char const* lib_name, char const* func_name, void** ptr_function)
{
	HMODULE hmod = LoadLibraryA(lib_name);
	FARPROC proc;

	if(NULL == hmod) return 0;

	proc = GetProcAddress(hmod, func_name);
	if(NULL == proc) return 0;

	*((FARPROC*)ptr_function) = proc;
	return 1;
}

#else

#define LIB_FILE_PREFIX "lib"
#define LIB_FILE_SUFFIX ".so"


#include <dlfcn.h>

static int load_function_from_shared_lib(char const* lib_name, char const* func_name, void** ptr_function)
{
	void* lib = dlopen(lib_name, RTLD_NOW | RTLD_GLOBAL);
	void* sym;

	if(NULL == lib)
    {
        printf("%s\n", dlerror());
        return 0;
    }

	sym = dlsym(lib, func_name);
	if(NULL == sym)
    {
        printf("%s\n", dlerror());
        return 0;
    }

	*ptr_function = sym;
	return 1;
}

#endif

typedef int(* function_ptr_t)(int);

int main()
{
	char const* lib_name1 = LIB_FILE_PREFIX "shared_lib1" LIB_FILE_SUFFIX;
	char const* lib_name2 = LIB_FILE_PREFIX "shared_lib2" LIB_FILE_SUFFIX;

	char const* func_name1 = "shared_lib1_function";
	char const* func_name2 = "shared_lib2_function";

	function_ptr_t func1, func2;

	printf("Loading shared-lib-function %s::%s\n", lib_name1, func_name1);
	if(!load_function_from_shared_lib(lib_name1, func_name1, (void**)&func1))
    {
        printf("Failed\n");
        return 1;
    }

	printf("Loading shared-lib-function %s::%s\n", lib_name2, func_name2);
	if(!load_function_from_shared_lib(lib_name2, func_name2, (void**)&func2))
    {
        printf("Failed\n");
        return 1;
    }
   
	printf("Executing shared-lib-function #1\n");
	func1(42);

	printf("Executing shared-lib-function #2\n");
	func2(42);

	return 0;
}