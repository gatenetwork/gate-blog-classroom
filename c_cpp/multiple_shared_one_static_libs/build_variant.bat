@ECHO OFF
del /S /Q "build\%1" >NUL
mkdir "build\%1" >NUL
cmake -B "build\%1" .
cmake --build "build\%1" --config Release
