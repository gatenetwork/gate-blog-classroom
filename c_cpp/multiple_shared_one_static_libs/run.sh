#!/bin/sh

echo "Building variant #1"
./build_variant.sh build_1
sleep 1
echo "Building variant #2"
./build_variant.sh build_2

rm -rf "build/1"
mkdir -p "test/1"

rm -rf "build/2"
mkdir -p "test/2"

cp -a "./build/build_1/output/." "./test/1"
cp -a "./build/build_2/output/." "./test/2"
echo "----------------------------------------"

echo "Run TEST 1 without modification"
LD_LIBRARY_PATH=./test/1 ./test/1/multiple_shared_one_static_libs
echo "----------------------------------------"

echo "Run TEST 2 without modification"
LD_LIBRARY_PATH=./test/2 ./test/2/multiple_shared_one_static_libs
echo "----------------------------------------"

echo "Modifying ./test/1/libshared_lib1.so"
cp "./build/build_2/output/libshared_lib1.so" "./test/1"
echo "Modifying ./test/2/libshared_lib1.so"
cp "./build/build_1/output/libshared_lib1.so" "./test/2"
echo "----------------------------------------"

echo "Run TEST 1 with modification"
LD_LIBRARY_PATH=./test/1 ./test/1/multiple_shared_one_static_libs
echo "----------------------------------------"

echo "Run TEST 2 with modification"
LD_LIBRARY_PATH=./test/2 ./test/2/multiple_shared_one_static_libs
echo "----------------------------------------"
