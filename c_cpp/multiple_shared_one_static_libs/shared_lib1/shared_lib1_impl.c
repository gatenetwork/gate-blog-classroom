#include "shared_lib1.h"

#include "static_lib/static_lib.h"

int shared_lib1_function(int param)
{
	return static_lib_function(param);
}