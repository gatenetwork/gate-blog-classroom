#ifndef SAMPLE_SHARED_LIB1_H_INCLUDED
#define SAMPLE_SHARED_LIB1_H_INCLUDED

#if defined(_WINDOWS) && defined(shared_lib1_EXPORTS)
__declspec(dllexport)
#endif

int shared_lib1_function(int param);

#endif
