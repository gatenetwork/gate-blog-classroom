
add_library(shared_lib2 SHARED
    shared_lib2.h
    shared_lib2_impl.c
)

target_link_libraries(shared_lib2
  static_lib
)
