#ifndef SAMPLE_SHARED_LIB2_H_INCLUDED
#define SAMPLE_SHARED_LIB2_H_INCLUDED

#if defined(_WINDOWS) && defined(shared_lib2_EXPORTS)
__declspec(dllexport)
#endif

int shared_lib2_function(int param);

#endif
