#!/bin/sh

if [ $# -eq 0 ]; then
    echo "Missing variant name argument"
    exit 1
fi


mkdir -p "build/$1"
rm -rf "build/$1"
cmake -B "build/$1" .
cmake --build "build/$1"
