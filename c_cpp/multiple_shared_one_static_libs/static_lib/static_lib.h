#ifndef SAMPLE_STATIC_LIB_H_INCLUDED
#define SAMPLE_STATIC_LIB_H_INCLUDED

#if defined(_WINDOWS)
__declspec(dllexport)
#endif

int static_lib_function(int param);

#endif