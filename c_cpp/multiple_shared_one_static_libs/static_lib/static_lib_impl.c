#include "static_lib.h"

#include "stdio.h"

int static_lib_function(int param)
{
	int result = param * 2;
	char const* msg = __DATE__ " " __TIME__;
	printf("Message from static_lib_function: %s\n", msg);
	return result;
}