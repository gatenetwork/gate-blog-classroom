# GCC static libraries in shared library

## Behavior analysis

Running GCC without specific compiler flags marks all functions as
*visible* and exports them in shared libraries (`*.so`).

This is also true for functions of static libraries (`*.a`) 
included in shared libraries.

Windows and MSVC compilers work different. Functions must be explicitely 
exported to show up in shared libraries (`*.dll`). Static libraries (`*.lib`) 
are not part of this contract and should not be published by a shared library.

When two shared libraries linked against the same static library are
loaded into one process, a GCC Linux environment will find two 
symbols of the same published static function and selects only one
of them for both libraries.

In MSVC Windows builds both shared libraries have their own private
copy of such a shared library that should not interfere.

## Problem

Static libraries are not published in software releases.
A shared library linked against a specific static-lib variant might
accidentally influence other shared libraries linked against
other versions or implementations of the same static libraries in one
process.

Portable software (e.g. for Windows and Linux) will react differently
or unexpected while compiling the same source code due to function 
symbol overlapping in GCC build environments.

## Test scenario

- We create two shared libraries linked against one static library that 
  provides a function printing its build time.
- The shared libraries publish one function that just invokes the static
  library's function.
- A test executable loads both shared libraries and invokes both exported
  functions from them.
- In one build (build #1), both functions invocation print the same build-timestamp
  as they use the same static library's code.
- Now we create another build (build #2) of the static and shared libraries 
  where the static library implements another build timestamp than in the
  first build process.
- We copy one of the shared libraries from the second build (build #2) 
  over the same file within the first build (build #1).
- Running the test executable of the first build (build #1) will show:
  - GCC build will print the same timestamp twice, both shared libs 
    invoke the "shared" static libraries function.
  - MSVC build will print different timestamps, as each shared library
    has its own private copy of the linked static library's implementation.

## Solution

Using compiler flag `-fvisibility=hidden` ensures in GCC environments
a similar behavior as on Windows/MSVC. A function must explicitely be
declared as *public* to be visible. This can be done by the 
*visibility "default" attribute*.

e.g.: 
`__attribute__((visibility ("default"))) int public_function(int param);`
 
 