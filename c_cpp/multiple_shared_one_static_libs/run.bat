@ECHO OFF

echo Building variant #1
call build_variant.bat build_1
timeout 1

echo ----------------------------------------
echo Building variant #2
call build_variant.bat build_2

del /S /Q "test\1" >NUL
mkdir "test\1"
del /S /Q "test\2" >NUL
mkdir "test\2"

copy "build\build_1\output\Release\*.*" "test\1"
copy "build\build_2\output\Release\*.*" "test\2"
echo ----------------------------------------

echo "Run TEST 1 without modification"
test\1\multiple_shared_one_static_libs
echo ----------------------------------------

echo Run TEST 2 without modification
test\2\multiple_shared_one_static_libs
echo ----------------------------------------

echo Modifying test\1\shared_lib1.dll
copy "build\build_2\output\Release\shared_lib1.dll" "test\1"
echo Modifying test\2\shared_lib1.dll
copy "build\build_1\output\Release\shared_lib1.dll" "test\2"
echo ----------------------------------------

echo Run TEST 1 with modification
test\1\multiple_shared_one_static_libs
echo ----------------------------------------

echo Run TEST 2 with modification
test\2\multiple_shared_one_static_libs
echo ----------------------------------------

pause