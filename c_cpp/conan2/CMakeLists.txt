cmake_minimum_required(VERSION 3.15)

project(hello_wikipedia)

find_package(boost REQUIRED)

include_directories(${Boost_INCLUDE_DIRS})

add_executable(${PROJECT_NAME}
  src/main.cpp
)

target_link_libraries(${PROJECT_NAME} 
  ${Boost_LIBRARIES}
)