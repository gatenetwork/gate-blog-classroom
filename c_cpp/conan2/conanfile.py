from conan import ConanFile
from conan.tools.cmake import CMake

class HelloWikipedia(ConanFile):
    name = "hello_wikipedia"
    version = "1.0"
    package_type = "application"

    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    exports_sources = "CMakeLists.txt", "src/*"
    requires = "boost/1.80.0"
    build_policy = "missing"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
