#include <iostream>
#include <boost/format.hpp>

int main()
{
  std::cout << boost::format("Hello %1%") % "Wikipedia"; 
  return 0;
}