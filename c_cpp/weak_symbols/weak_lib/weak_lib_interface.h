#ifndef WEAK_LIB_INTERFACE_H_INCLUDED
#define WEAK_LIB_INTERFACE_H_INCLUDED

int public_function(int param);

int interface_function(int param) __attribute__((weak));

#endif
