#!/bin/sh

BUILD_FOLDER="build"

echo $BUILD_FOLDER
rm -rf $BUILD_FOLDER
mkdir -p $BUILD_FOLDER
cd $BUILD_FOLDER
cmake ..
make
cd ..