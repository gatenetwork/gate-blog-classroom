
add_executable(weak_symbols_loader
  main.c
)

target_link_libraries(weak_symbols_loader
  weak_lib
)
