#include <weak_lib/weak_lib_interface.h>

#include <stdio.h>

int main()
{
	int sample_value = public_function(12);

	printf("Return value from public function: %d\n", sample_value);

	return 0;
}