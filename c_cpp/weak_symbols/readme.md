# GCC weak symbols in linked libraries

## Behavior analysis

Functions and variables (or symbols in general) can be attributed with the 
`weak` flag.

```
int my_function(int param) __attribute__((weak));

int my_global_var __attribute__((weak));
```

It allows a library to define and export addressable functions and variables,
but without the need to implement them.

When the same symbol name is load from another library (without the `weak` 
attribute), the previous weak symbol is overwritten and points to the 
new imported symbol.

A interface library can therefore define public functions that are not
implemented, and the interface library itself is linked to an implementation
library. When a process loads the interface library, it also loads the
linked implementation library and therefor the `weak` symbols in the
interface library become defined by symbols from the implementation library.

## Problem

New GCC versions are scanning libraries for their function calls and 
can detect if exported functions from a library are never called.
In that case they will remove such library from the list dependent
linked libraries.

An interface library with `weak` symbols without direct invocations of
symbols in the implementation library will be cut down to run stand-alone.

When a process links the interface library and calls its `weak` functions,
a NULL-pointer segmentation fault will be raised as no implementation
has overwritten `weak` symbols.

## Solution

The linker flag `-no-as-needed` enforces new compiler to link all given
libraries.

## Test scenario

TBD
 