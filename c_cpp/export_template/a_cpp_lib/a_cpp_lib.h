#ifndef A_CPP_LIB_H_INCLUDED
#define A_CPP_LIB_H_INCLUDED

#include <utility>

#if defined(a_cpp_lib_EXPORTS)
#  if defined(_MSC_VER)
#    define LIB_API __declspec(dllexport)
#    define LIB_EXTERN
#  else
#    define LIB_API __attribute__((visibility ("default")))
#    define LIB_EXTERN extern
#  endif
#else
#  if defined(_MSC_VER)
#    define LIB_API  __declspec(dllimport)
#    define LIB_EXTERN extern
#  else
#    define LIB_API
#    define LIB_EXTERN extern
#  endif
#endif

template<class T> using Complex = std::pair<T, T>;

// declare export of some std::pair instances
LIB_EXTERN template struct LIB_API std::pair<int, int>;
LIB_EXTERN template struct LIB_API std::pair<double, double>;


template<class T>
class LIB_API Number
{
public:
  Number(T const& t = T());

  T const& get() const;
  void set(T const& t);
  void print() const;

private:
  T value;
};

LIB_EXTERN template class LIB_API Number<int>;
LIB_EXTERN template class LIB_API Number<double>;
LIB_EXTERN template class LIB_API Number<Complex<int> >;
LIB_EXTERN template class LIB_API Number<Complex<double> >;


#endif
