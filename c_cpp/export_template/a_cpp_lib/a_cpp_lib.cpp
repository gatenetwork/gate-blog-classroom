#include "a_cpp_lib.h"
#include <iostream>

template struct std::pair<int, int>;
template struct std::pair<double, double>;

template<class K, class V>
std::ostream& operator<<(std::ostream& strm, std::pair<K, V> const& p)
{
  strm << "(" << p.first << "," << p.second << ")"; 
  return strm;
}


template<class T> Number<T>::Number(T const& t)
: value(t)
{
}

template<class T> T const& Number<T>::get() const
{
  return this->value;
}

template<class T> void Number<T>::set(T const& t)
{
  this->value = t;
}

template<class T> void Number<T>::print() const
{
  std::cout << this->value << std::endl;
}


template class Number<int>;
template class Number<double>;
template class Number<Complex<int> >;
template class Number<Complex<double> >;

