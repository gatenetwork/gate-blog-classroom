cmake_minimum_required(VERSION 3.0)
project(export_template)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/output" CACHE PATH "Shared library target directory")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/output" CACHE PATH "Static library target directory")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/output" CACHE PATH "Executable target directory")

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")

add_subdirectory(a_cpp_lib)
add_subdirectory(a_cpp_app)
