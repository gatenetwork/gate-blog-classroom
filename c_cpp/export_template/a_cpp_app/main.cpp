#include <stdio.h>

#include "a_cpp_lib/a_cpp_lib.h"
#include <cassert>
#include <iostream>


int main()
{
	std::cout << "Begin" << std::endl;
	Number<int> a;
	a.set(42);
	assert(a.get() == 42);
	a.print();

	Number<double> b;
	b.set(42.42);
	assert(b.get() == 42.42);
	b.print();

	Number<Complex<int> > c;
	c.set(Complex<int>(1, 2));
	assert(c.get().first == 1);
	assert(c.get().second == 2);
	c.print();

	Number<Complex<double> > d;
	d.set(Complex<double>(1.2, 3.4));
	assert(d.get().first == 1.2);
	assert(d.get().second == 3.4);
	d.print();

	std::cout << "End" << std::endl;
	return 0;
}
