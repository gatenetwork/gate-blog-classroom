
add_executable(a_cpp_app
    main.cpp
)

target_link_libraries(a_cpp_app
  a_cpp_lib
)


if(UNIX)
  target_link_libraries(a_cpp_app
    dl
  )
endif()
