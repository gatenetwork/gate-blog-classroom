# Exporting and importing C++ template instances from shared libraries

Regular functions and objects can be are made exportable or importable
from shared libraries by special compiler attributes.
(e.g. `__declspec` on MSVC or `__attribute__((visibility))` on GCC)

C++ template instances need special preparation as the required
definition of is different those compilers.

## Declarations

- MSVC:
  - Export: `template class __declspec(dllexport) MyTemplate<type_t>;`
  - Import: `extern template class __declspec(dllimport) MyTemplate<type_t>;`
- GCC:
  - Export: `extern template class __attribute__((visibility ("default"))) MyTemplate<type_t>;`
  - Import: `extern template class MyTemplate<type_t>;`

To generalize declaration, we use macros.
As the `extern` prefix is forbidden on MSVC exports, we need two macros.
One to express the `extern` state, the other one for the 
`export/import/visibility` attribute.

A sample macro declaration can look like:

```cpp
#if defined(lib_EXPORTS)
#  if defined(_MSC_VER)
#    define LIB_API __declspec(dllexport)
#    define LIB_EXTERN
#  else
#    define LIB_API __attribute__((visibility ("default")))
#    define LIB_EXTERN extern
#  endif
#else
#  if defined(_MSC_VER)
#    define LIB_API  __declspec(dllimport)
#    define LIB_EXTERN extern
#  else
#    define LIB_API
#    define LIB_EXTERN extern
#  endif
#endif

LIB_EXTERN template class LIB_API MyTemplate<int>;
LIB_EXTERN template class LIB_API MyTemplate<double>;
```
## Test sample

- We create a shared library named `a_cpp_lib`
- We create an executable `a_cpp_app` linking against `a_cpp_lib`.
- `a_cpp_app` uses the headers from `a_cpp_lib`.
- `a_cpp_app` needs to import template instances that are exported from `a_cpp_lib`.
