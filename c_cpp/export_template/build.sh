#!/bin/sh
BUILD_FOLDER=./build
rm -rf $BUILD_FOLDER
mkdir -p $BUILD_FOLDER
cmake -B $BUILD_FOLDER
cmake --build $BUILD_FOLDER
