#include <bios.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#pragma pack( __push, 1 )

typedef struct 
{
    unsigned char   code[3];
    char            oem_name[8];
    unsigned short  bytes_per_sector;
    unsigned char   sectors_per_cluster;
    unsigned short  reserved_sectors;
    unsigned char   fat_copies;
    unsigned short  root_entries;
    unsigned short  sectors_total;
    unsigned char   media_descriptor;
    unsigned short  sectors_per_fat;
    unsigned short  sectors_per_track;
    unsigned short  heads;
    unsigned long   sectors_hidden;
    unsigned long   sectors_total_large;

    unsigned char   bios_drive_id;
    unsigned char   reserved;
    unsigned char   signature;
    unsigned long   filesystem_id;
    char            volume_id[11];
    char            fat_name[8];
    unsigned char   boot_loader[448];
    unsigned short  boot_sector_signature;
} fat12_info_t;

#pragma pack( __pop )

int print_disk_fat_info(unsigned drive)
{
    int ret = 0;
    unsigned short result;
    struct _ibm_diskinfo_t info;
    unsigned char buffer[512];
    char text[64];
    fat12_info_t* ptr_info;

    printf("Reading disk FAT info from drive %d\n", drive);
    assert(sizeof(fat12_info_t) == 256);
    memset(buffer, 0, sizeof(buffer));
    memset(&info, 0, sizeof(info));

    info.drive = drive;
    info.head = 0;
    info.track = 0;
    info.sector = 1;
    info.nsectors = 1;
    info.buffer = &buffer[0];

    /*
    result = _bios_disk(_DISK_RESET, &info);
    if(result != 0)
    {
        printf("Failed to reset disk, Error %d", result);
        return 1;
    }
    */

    result = _bios_disk(_DISK_READ, &info);
    if(result != 0)
    {
        printf("Failed to read boot sector, Error %d\n", result);
        ret = 1;
    }

    ptr_info = (fat12_info_t*)&buffer[0];
    memset(text, 0, sizeof(text));
    memcpy(text, ptr_info->oem_name, sizeof(ptr_info->oem_name));
    printf("OEM Name: %s\n", text);
    printf("Bytes per sector: %d\n", ptr_info->bytes_per_sector);
    printf("Sectors per cluster: %d\n", ptr_info->sectors_per_cluster);
    printf("Reserved sectors: %d\n", ptr_info->reserved_sectors);
    printf("Copies of FAT: %d\n", ptr_info->fat_copies);
    printf("Root filesystem entries: %d\n", ptr_info->root_entries);
    printf("Total sectors: %d\n", ptr_info->sectors_total);
    printf("Media descriptor byte: %x\n", (unsigned)ptr_info->media_descriptor);
    printf("Sectors per FAT: %d\n", ptr_info->sectors_per_fat);
    printf("Sectors per track: %d\n", ptr_info->sectors_per_track);
    printf("Heads: %d\n", ptr_info->heads);
    printf("Hidden sectors: %d\n", ptr_info->sectors_hidden);
    printf("BIOS drive ID: %d\n", ptr_info->bios_drive_id);
    printf("Signature Byte: %x\n", (unsigned)ptr_info->signature);
    printf("Filesystem ID: %x\n", (unsigned)ptr_info->filesystem_id);
    printf("Boot sector signature: %x\n", (unsigned)ptr_info->boot_sector_signature);

    memset(text, 0, sizeof(text));
    memcpy(text, ptr_info->fat_name, sizeof(ptr_info->fat_name));
    printf("FAT name: %s\n", text);
    return ret;
}

int main(int argc, char* argv[])
{
    int ret = 0;
    unsigned int drive_id = 0;
    if(argc >= 2)
    {
        sscanf(argv[1], "%d", &drive_id);
    }
    ret = print_disk_fat_info(drive_id);
    return ret;
}
