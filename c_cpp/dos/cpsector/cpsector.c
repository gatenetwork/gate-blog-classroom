#include <bios.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#pragma pack( __push, 1 )

int copy_nsectors(FILE* outfile, unsigned drive, unsigned head, unsigned track, unsigned start_sector, unsigned sector_count)
{
    struct _ibm_diskinfo_t info;
    unsigned char buffer[512 * 36];
    unsigned result;
    size_t length = 512 * sector_count;
    size_t written;

    if(sector_count > 36)
    {
        printf("Too much sectors\n");
        return 1;
    }

    memset(&buffer[0], 0, sizeof(buffer));
    memset(&info, 0, sizeof(info));

    info.drive = drive;
    info.head = head;
    info.track = track;
    info.sector = start_sector;
    info.nsectors = sector_count;
    info.buffer = &buffer[0];

    result = _bios_disk(_DISK_RESET, &info);

    result = _bios_disk(_DISK_READ, &info);

    written = fwrite(&buffer[0], sizeof(buffer[0]), length, outfile);
    if(written < length)
    {
        printf("Failed to write to file, %d of %d bytes transferred\n", written, length);
        return 1;
    }
    return 0;
}

int copy_sectors(FILE* outfile, unsigned drive, unsigned head, unsigned track, unsigned sector, unsigned char count)
{
    int ret = 0;
    printf("Copy drive %d, head %d, track %d, sector %d (%d sectors)\n", drive, head, track, sector, count);
    ret = copy_nsectors(outfile, drive, head, track, sector, count);
    return ret;
}

int main(int argc, char* argv[])
{
    char const* filename = NULL;
    unsigned int drive_id = 0;
    unsigned int head = 0;
    unsigned int track = 0;
    unsigned int sector = 0;
    unsigned int count = 0;
    FILE* outfile = NULL;
    int ret = 0;

    if(argc < 7)
    {
        printf("Usage: cpsector [outfile] [drivenum] [head] [track] [startsector] [sectorcount]\n");
        return 1;
    }
    filename = argv[1];
    sscanf(argv[2], "%d", &drive_id);
    sscanf(argv[3], "%d", &head);
    sscanf(argv[4], "%d", &track);
    sscanf(argv[5], "%d", &sector);
    sscanf(argv[6], "%d", &count);

    outfile = fopen(filename, "wb");
    if(outfile == NULL)
    {
        printf("Failed to open output file\n");
        return 2;
    }

    ret = copy_sectors(outfile, drive_id, head, track, sector, count);

    fclose(outfile);
    if(ret == 0)
    {
        printf("Completed\n");
    }
    else
    {
        printf("Aborted\n");
    }

    return ret;
}
