#include <dos.h>
#include <stdio.h>

void print_attrib(unsigned isset, char const* token)
{
    if(isset)
    {
        printf("%s", token);
    }
    else
    {
        printf("-");
    }
}

int list_dir(char const* path)
{
    unsigned seach_attribs = _A_NORMAL | _A_RDONLY | _A_HIDDEN | _A_SYSTEM | _A_SUBDIR;
    struct find_t entry;
    unsigned result;
    result = _dos_findfirst(path, seach_attribs, &entry);
    if(result == 0)
    {
        while(result == 0)
        {
            print_attrib(entry.attrib & _A_SUBDIR, "D");
            print_attrib(entry.attrib & _A_RDONLY, "R");
            print_attrib(entry.attrib & _A_HIDDEN, "H");
            print_attrib(entry.attrib & _A_SYSTEM, "S");
            print_attrib(entry.attrib & _A_ARCH, "A");
            printf("%14s", entry.name);
            if(entry.attrib & _A_SUBDIR)
            {
                printf("%12s", "<DIR>");
            }
            else
            {
                printf("%12ld", entry.size);
            }
            printf("\n");
            result = _dos_findnext(&entry);
        }
        _dos_findclose(&entry);
        return 0;
    }
    else
    {
        printf("Error #%d\n", result);
        return 1;
    }
}

int main(int argc, char* argv[])
{
    int ret = 0;
    if(argc < 2)
    {
        ret = list_dir("*.*");
    }
    else
    {
        ret = list_dir(argv[1]);
    }
    return ret;
}