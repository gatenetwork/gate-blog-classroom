@ECHO OFF

IF EXIST "C:\WATCOM\owsetenv.bat" (
  CALL "C:\WATCOM\owsetenv.bat"
)

SET OLDDIR=%CD%
rmdir /s /q build
mkdir build
cd build
cmake -G "Watcom WMake" -DCMAKE_SYSTEM_NAME=DOS -DCMAKE_SYSTEM_PROCESSOR=I86 -DCMAKE_BUILD_TYPE=Release ..\
SET CL=/MP
wmake
CHDIR /d %OLDDIR%
