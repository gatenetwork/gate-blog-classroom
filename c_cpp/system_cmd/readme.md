# System command execution (system_cmd)

The C `stdlib` function `system(char const* command)` takes a string and executes it as
a native shell command.  
The `system_cmd` demo utility displays all its given process arguments and takes
the first process argument (index = 1) and passes it as string to `system()`.

## Shell argument transformation

The default implementation of `system(command)` employs the standard shell of an
operating system to execute the given command.  
- Linux: 
  - Process argument tokens:
    - `/bin/sh`
    - `-c`
    - `command`
- Windows: 
  - Process argument tokens:
    - `cmd.exe`
    - `/c`
    - `command`
  - Full command line: `cmd.exe /c "` + `command` + `"`

This means, the execution of:  
`system_cmd "echo Hello World"`  
will be transformed and executed as:  
`/bin/sh -c "echo Hello World"`

### Program usage

`system_cmd "argument1"`  
will print all given arguments and run C `system()` function with `argument1` afterwards.

Example:

```
$ ./system_cmd "echo Hello World"
Program: ./system_cmd
Argument 1: echo Hello World
Executing command: echo Hello World
--------------------
Hello World

--------------------
Exit code: 0
```

## Escape tokens

POSIX shells distinguish between single and double quoted arguments.  
Double quoted arguments are parsed to substitute contained variables and to apply escape sequences.
Single quoted arguments are preserved as they are, so no variable substitution is performed.

## Demo setup

`system_cmd` can be used to invoke itself with another command.
Each process shows the exact arguments it receives from the parent process (shell).
Shifting arguments to another process opens a second chance to resolve and 
substitute argument contents in the seconds shell session.

### Demo 1: Linux - double quotes outside substitute variables

```
$ ./system_cmd "./system_cmd 'echo $HOME'"
```

Invoking the outer `system_cmd` from the shell with variable `$HOME` shows
that the outer process receives already the substituted home path, which is forwarded
to the inner process that uses `echo` to print it.

### Demo 2: Linux - single quotes outside do not substitute variables

```
$ ./system_cmd './system_cmd "echo $HOME"'
```

The outer process receives the `system_cmd "echo $HOME"` command.
When the outer process starts the inner one, `$HOME` is substituted by the
real home path and printed the console.

### Demo 3: Windows -

```
> system_cmd "system_cmd \"echo %USERNAME%\""
```

Variable substitution is done before the first process starts.

### Demo 4: Windows -

```
system_cmd "system_cmd \"echo "%%"USERNAME"%%\""
```

Variable is broken in pieces to be ignored outside.
The second process receives the substituted content from the shell
execution of the first process.