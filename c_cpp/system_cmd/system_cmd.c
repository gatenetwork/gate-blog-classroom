#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	int n;
	char const* system_arg = "";
	int exit_code;

	printf("Program: %s\n", argv[0]);
	for(n = 1; n < argc; ++n)
	{
		printf("Argument %d: %s\n", n, argv[n]);
	}
	if(argc >= 2)
	{
		system_arg = argv[1];
	}
	printf("Executing command: %s\n", system_arg);
	printf("--------------------\n");
	exit_code = system(system_arg);
	printf("\n--------------------\n");
	printf("Exit code: %d\n", exit_code);

	return exit_code;
}
