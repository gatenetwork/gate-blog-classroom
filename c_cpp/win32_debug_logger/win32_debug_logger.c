#include <stdio.h>
#include <stdarg.h>
#include <wchar.h>
#include <Windows.h>
#include <time.h>

typedef struct debugging_context
{
	HANDLE hProcess;
	DWORD dwThreadId;
} debugging_context_t;

static int print(char const* pattern, ...)
{
	int ret;
	va_list arg_ptr;

	va_start(arg_ptr, pattern);
	ret = vprintf(pattern, arg_ptr);
	va_end(arg_ptr);

	return ret;
}

static int wprint(wchar_t const* pattern, ...)
{
	int ret;
	va_list arg_ptr;

	va_start(arg_ptr, pattern);
	ret = vwprintf(pattern, arg_ptr);
	va_end(arg_ptr);

	return ret;
}

static int get_execution_time()
{
	static time_t start_time = 0;
	time_t current_time = time(NULL);
	if(start_time == 0)
	{
		start_time = current_time;
	}
	return (int)(current_time - start_time);
}

static int print_time()
{
	int execution_time = get_execution_time();
	return wprint(L"[T%+d] ", execution_time);
}

static int print_file_path_of_handle(HANDLE hfile)
{
	WCHAR buffer[4096];
	DWORD received = GetFinalPathNameByHandleW(hfile, &buffer[0], sizeof(buffer) / sizeof(buffer[0]) - 1, FILE_NAME_NORMALIZED);
	buffer[received] = 0;
	return wprint(L"%s", &buffer[0]);
}

static int print_thread(DWORD thread_id)
{
	return wprint(L"@T%d ", thread_id);
}

static int print_remote_string(debugging_context_t const* context, void* ptr, WORD length, int unicode)
{
	int chars_printed = 0;
	wchar_t buffer[65536 + 2];
	char* c_ptr = (char*)&buffer[0];
	wchar_t* w_ptr = (wchar_t*)&buffer[0];
	SIZE_T returned = 0;
	char* src_ptr = (char*)ptr;

	if(unicode)
	{
		if(ReadProcessMemory(context->hProcess, src_ptr, w_ptr, sizeof(wchar_t) * length, &returned))
		{
			w_ptr[returned] = 0;
			chars_printed += wprint(L"%s", w_ptr);
		}
	}
	else
	{
		if(ReadProcessMemory(context->hProcess, src_ptr, c_ptr, sizeof(char) * length, &returned))
		{
			c_ptr[returned] = 0;
			chars_printed += print("%s", *c_ptr);
		}
	}
	
	return chars_printed;
}
static int print_remote_image_path(debugging_context_t const* context, void* ptr, int unicode)
{
	int chars_printed = 0;
	char* remote_address = NULL;
	SIZE_T returned = 0;
	SIZE_T char_len = unicode ? sizeof(wchar_t) : sizeof(char);
	wchar_t buffer[4] = { 0 };
	char* ptr_char = (char*)&buffer[0];

	if(ReadProcessMemory(context->hProcess, ptr, &remote_address, sizeof(remote_address), &returned))
	{
		if(remote_address != NULL)
		{
			while(ReadProcessMemory(context->hProcess, remote_address, ptr_char, char_len, &returned))
			{
				if(unicode)
				{
					if(buffer[0] == 0) break;
					wprint(L"%c", buffer[0]);
				}
				else
				{
					if(*ptr_char == 0) break;
					print("%c", *ptr_char);
				}
				remote_address += char_len;
				++chars_printed;
			}
		}
	}
	return chars_printed;
}

static void print_load_dll_debug_info(debugging_context_t const* context, LOAD_DLL_DEBUG_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprint(L"LOAD_DLL_DEBUG_EVENT");
	wprint(L", hFile=%p", info->hFile);
	if(info->hFile)
	{
		wprint(L", path=");
		print_file_path_of_handle(info->hFile);
	}
	wprint(L", lpBaseOfDll=%p", info->lpBaseOfDll);
	if(info->lpImageName)
	{
		wprint(L", lpImageName=");
		print_remote_image_path(context, info->lpImageName, info->fUnicode);
	}
	wprint(L"\n");
}

static void print_unload_dll_debug_info(debugging_context_t const* context, UNLOAD_DLL_DEBUG_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprint(L"UNLOAD_DLL");
	wprint(L", lpBaseOfDll=%p", info->lpBaseOfDll);
	wprint(L"\n");
}

static void print_create_process_debug_info(debugging_context_t const* context, CREATE_PROCESS_DEBUG_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprint(L"CREATE_PROCESS");
	wprint(L", hFile=%p", info->hFile);
	if(info->hFile)
	{
		wprint(L", path=");
		print_file_path_of_handle(info->hFile);
	}
	wprint(L", hProcess=%p", info->hProcess);
	wprint(L", hThread=%p", info->hThread);
	if(info->lpImageName)
	{
		wprint(L", lpImageName=");
		print_remote_image_path(context, info->lpImageName, info->fUnicode);
	}
	wprint(L"\n");
}

static void print_exit_process_debug_info(debugging_context_t const* context, EXIT_PROCESS_DEBUG_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprintf(L"EXIT_PROCESS");
	wprintf(L", dwExitCode=%d", (int)info->dwExitCode);
	wprintf(L"\n");
}

static void print_create_thread_debug_info(debugging_context_t const* context, CREATE_THREAD_DEBUG_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprint(L"CREATE_THREAD");
	wprint(L", hThread=%p", info->hThread);
	wprint(L", lpStartAddress=%p", info->lpStartAddress);
	wprint(L", lpThreadLocalBase=%p", info->lpThreadLocalBase);
	wprint(L"\n");
}

static void print_exit_thread_debug_info(debugging_context_t const* context, EXIT_THREAD_DEBUG_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprint(L"EXIT_THREAD");
	wprint(L", dwExitCode=%d", info->dwExitCode);
	wprint(L"\n");
}

static void print_exception_debug_info(debugging_context_t const* context, EXCEPTION_DEBUG_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprint(L"EXCEPTION");
	wprint(L", ExceptionCode=%d", (int)info->ExceptionRecord.ExceptionCode);
	wprint(L", ExceptionFlags=%d", (int)info->ExceptionRecord.ExceptionFlags);
	wprint(L", ExceptionAddress=%p", info->ExceptionRecord.ExceptionAddress);
	wprint(L"\n");
}

static void print_output_debug_string(debugging_context_t const* context, OUTPUT_DEBUG_STRING_INFO const* info)
{
	char chr_buffer[65536 + 2];
	wchar_t* wchr_ptr = (wchar_t*)&chr_buffer[0];
	WORD length = (WORD)info->nDebugStringLength;

	print_time();
	print_thread(context->dwThreadId);
	wprint(L"OUTPUT_DEBUG_STRING=");

	memcpy(chr_buffer, info->lpDebugStringData, length);

	if(info->fUnicode)
	{
		wchr_ptr[length / 2] = 0;
		wprint(L"%s", wchr_ptr);
	}
	else
	{
		chr_buffer[length] = 0;
		print("%s", chr_buffer);
	}
	wprint(L"\n");
}

static void print_rip_info(debugging_context_t const* context, RIP_INFO const* info)
{
	print_time();
	print_thread(context->dwThreadId);
	wprint(L"RIP_EVENT");
	wprint(L", dwType=%d", (int)info->dwType);
	wprint(L", dwError=%d", (int)info->dwError);
	wprint(L"\n");
}



int run_with_debugging(wchar_t* command_line)
{
	STARTUPINFOW startup_info = { 0 };
	PROCESS_INFORMATION process_info = { 0 };
	DEBUG_EVENT debug_event = { 0 };

	debugging_context_t context = { 0 };

	if(!CreateProcessW(NULL, command_line, NULL, NULL, FALSE, 
					   DEBUG_PROCESS, NULL, NULL,
					   &startup_info, &process_info
	))
	{
		wprintf(L"CreateProcessW() failed: %d\n", (int)GetLastError());
		return 3;
	}
	
	context.hProcess = process_info.hProcess;
	context.dwThreadId = process_info.dwThreadId;

	for(;;)
	{
		if(!WaitForDebugEvent(&debug_event, INFINITE))
		{
			wprintf(L"\nWaitForDebugEvent() failed, %d\n", (int)GetLastError());
			return 4;
		}

		context.dwThreadId = debug_event.dwThreadId;

		switch(debug_event.dwDebugEventCode)
		{
		case LOAD_DLL_DEBUG_EVENT:
			{
				print_load_dll_debug_info(&context, &debug_event.u.LoadDll);
				break;
			}
		case UNLOAD_DLL_DEBUG_EVENT:
			{
				print_unload_dll_debug_info(&context, &debug_event.u.UnloadDll);
				break;
			}
		case CREATE_PROCESS_DEBUG_EVENT:
			{
				print_create_process_debug_info(&context, &debug_event.u.CreateProcessInfo);
				break;
			}
		case EXIT_PROCESS_DEBUG_EVENT:
			{
				print_exit_process_debug_info(&context, &debug_event.u.ExitProcess);
				return 0;
			}
		case CREATE_THREAD_DEBUG_EVENT:
			{
				print_create_thread_debug_info(&context, &debug_event.u.CreateThread);
				break;
			}
		case EXIT_THREAD_DEBUG_EVENT:
			{
				print_exit_thread_debug_info(&context, &debug_event.u.ExitThread);
				break;
			}
		case EXCEPTION_DEBUG_EVENT:
			{
				print_exception_debug_info(&context, &debug_event.u.Exception);
				break;
			}
		case OUTPUT_DEBUG_STRING_EVENT:
			{
				print_output_debug_string(&context, &debug_event.u.DebugString);
				break;
			}
		case RIP_EVENT:
			{
				print_rip_info(&context, &debug_event.u.RipInfo);
				break;
			}
		}

		if(!ContinueDebugEvent(debug_event.dwProcessId, debug_event.dwThreadId, DBG_CONTINUE))
		{
			wprint(L"\nContinueDebugEvent() failed, %d\n", (int)GetLastError());
			return 5;
		}
	}

	return 0;
}

int wmain(int argc, wchar_t* argv[])
{
	wchar_t buffer[4096 + 2048] = { 0 };
	wchar_t* ptr = &buffer[0];
	int n;
	int printed;

	if(argc < 2)
	{
		/* no arguments */
		return 1;
	}

	for(n = 1; n < argc; ++n)
	{
		printed = swprintf(ptr, sizeof(buffer), L" \"%s\"", argv[n]);
		if(printed < 0)
		{
			/* error */
			return 2;
		}
		ptr += printed;
	}

	/* first character is a [space], skip it! */
	ptr = &buffer[1];

	wprint(L"Starting process with debugging %s\n", ptr);
	return run_with_debugging(ptr);
}

