# Win32 Debug Logger

This small console program starts another process in debug mode and receives
debugging message from it. 
All messages are printed onto the attached console.

The origin of the idea to this program was an `OpenCppCoverage`
[issue regarding Docker](https://github.com/OpenCppCoverage/OpenCppCoverage/issues/58).


## Prerequisites

- CMake v3.0+
- Microsoft Visual Studio 2008 or newer.
- Windows Vista+ SDK


## Build script

Invoke `build_with_cmake.bat` to run `cmake` to build to provided source 
codes. The resulting executable named `win32_debug_logger`
will be placed in `/bin/Release` output folder.


## Usage

`win32_debug_logger.exe other.exe other_exe_param1 param2`


## Research regarding Docker and WaitForDebugEvent API

- `LOAD_DLL` event's `hFile` is `NULL`, when the module is located on a
  volume mapping folder between docker and its host.
- `hFile` is also `NULL`, when the executed command is coming from a 
  shared docker layer (e.g. `c:\\Windows\\System32\\cmd.exe`)
- `hFile` receives a valid handle, when the module is stored on the running 
  container layer.  
  e.g. copy all EXE/DLL files to a new created folder like `c:\test` within
  the docker container.
- `lpImageName` is in most cases pointing to a valid string in the debugged
  process. `ReadProcessMemory` can extract the full path. (Only some libraries
  like `ntdll` have no valid image name reference).
