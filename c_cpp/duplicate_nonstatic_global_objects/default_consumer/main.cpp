/*
 * This is a default consumer program of shared_lib
 *
 * - It loads shared_lib.
 * - It executes a function of shared_lib.
 * - That's it.
 */

#include "shared_lib/shared_lib_interface.h"
#include <iostream>

int main()
{
	std::cout << "Init shared_lib" << std::endl;
	public_shared_lib_init();

	std::cout << "Invoke shared_lib" << std::endl;
	int some_status = public_shared_lib_function(42);

	std::cout << "Exit program with status: " << some_status << std::endl;
	return some_status;
}
