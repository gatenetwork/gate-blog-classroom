#!/bin/sh
echo "----- BUILDING with private globals as STATIC variables"
./build_static.sh

echo "----- EXECUTING test using STATIC variables"
chdir ./build_static/output/
./test
chdir ../../
echo "########## No crash expected, all should be fine ;) ##########"


echo "----- BUILDING with private globals as NON-STATIC variables"
./build_nonstatic.sh

echo "----- EXECUTING test using NON-STATIC variables"
chdir ./build_nonstatic/output/
./test
chdir ../../
echo "########## Did you see the crash above ? ##########"

