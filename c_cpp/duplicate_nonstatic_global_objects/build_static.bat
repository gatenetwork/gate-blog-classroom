@ECHO OFF
SET BUILD_FOLDER="build_static"
del /S /Q %BUILD_FOLDER% >NUL
mkdir %BUILD_FOLDER% >NUL
cmake -D USE_STATIC_FOR_PRIVATE_VARS=1 -B %BUILD_FOLDER%
cmake --build %BUILD_FOLDER% --config Release
