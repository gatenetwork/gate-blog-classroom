cmake_minimum_required(VERSION 3.0)
project(duplicate_nonstatic_global_objects)


set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/output" CACHE PATH "Shared library target directory")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/output" CACHE PATH "Static library target directory")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/output" CACHE PATH "Executable target directory")

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")

add_subdirectory(shared_lib)
add_subdirectory(default_consumer)
add_subdirectory(test_consumer)
