#!/bin/sh
export BUILD_FOLDER="./build_static"
rm -rf $BUILD_FOLDER
mkdir -p $BUILD_FOLDER
cmake -D USE_STATIC_FOR_PRIVATE_VARS=1 -B $BUILD_FOLDER
cmake --build $BUILD_FOLDER
