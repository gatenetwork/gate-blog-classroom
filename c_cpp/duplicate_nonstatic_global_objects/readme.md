# GCC duplicate non-static global objects

## Behavior analysis

If an executable imports code from a shared library AND code from a static
library using the same global variables, GCC merges both variable 
addresses into one location.

Unfortunately, GCC does not remove duplicate codes for construction or
destruction of data types. 

This results in double construction and double destruction of global objects
on only one memory location.
Construction will in most cases "only" produce memory leaks, but destruction
raises following error:

> free(): double free detected
> Aborted (core dumped)

The exit code of aborted binaries is unstable and destruction of other 
global objects might become omitted.

## Problem

Global C++ objects have special global constructor and destructor functions
that are called on process creation and process terminations.

When two global objects are merged into one memory location (becoming only 
one single global object), the first global destructor destroys this 
global object.  
The second destructor operation on the already freed object and will see
garbage bits. This might (or also might not) lead to segmentation faults 
or other errors when garbage memory is accessed.

The problem 

*NOTICE:* This is a GCC problem. 
MSVC does not apply this kind of variable merges.

## Test scenario

The test scenario is based on some kind of whitebox test, where a test
executable directly includes code from a shared library to invoke 
private functions. In parallel the final shared library is loaded by
the test process too.  
This results in merging global variables from both include variants
and generates only ONE memory object, but two constructor and destructor
functions for them,

- We create a shared library named `shared_lib`.  
  It internally uses a global C++ variable of type std::string to store states.
- We create a demo-consume executable just to show, that we can load and
  use `shared_lib` functions.
- We create a test executable that includes CPP files from the `shared_lib`
  to invoke private functions to modify the global state variable.
  And the test executable links against `shared_lib`
- On process termination of the test executable we receive a crash when
  the global std::string state variable is going to be destructed twice.

## Solution

If private global variables are marked as `static` and if they are only 
accessed by public or internal functions within their compilation units, 
no variable merges are performed.  

As there is currently no way to remove double destructor generation,
duplicating the variable is the only chance to get rid of crashes
on process shutdown.

## Test scenarios with STATIC vs. NON-STATIC variables 

We are compiling the test environment twice.

- One build is called `NON-STATIC` and shows the crash due to variable 
  space merges.
- The second build is called `STATIC` and makes the global variables `static`
  to fix and prohibit variable space merges.

