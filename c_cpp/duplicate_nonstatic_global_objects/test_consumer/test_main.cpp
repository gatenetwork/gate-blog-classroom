/* 
 * SAMPLE whitebox unit test
 *
 * - Import private functions from library by source 
 * - Invokes private functions
 * - Evaluate results.
 */

#include "shared_lib/shared_lib_interface.h"
#include "shared_lib/shared_lib_details.h"

#include <iostream>

int main()
{
	std::cout << "Init shared_library" << std::endl;
	public_shared_lib_init();	


	static std::string const expected_text("Some value to test");

	std::cout << "Set test content" << std::endl;
	private_set_global_variable_content(expected_text.c_str());

	std::cout << "Retrieve test content" << std::endl;
	std::string retrieved_text = private_get_global_variable_content();

	std::cout << "Check test content" << std::endl;
	bool test_succeeded = (retrieved_text == expected_text);

	if(test_succeeded)
	{
		std::cout << "Test succeeded" << std::endl;
		return 0;
	}
	else
	{
		std::cout << "Test failed" << std::endl;
		return 1;
	}
}
