/*
 * This is a sample shared library named shared_lib
 *
 * - It exports two public functions, one for initialization, on for same function
 * - In its implementation details, it uses a global state variable
 */


#include "shared_lib/shared_lib_interface.h"
#include "shared_lib/shared_lib_details.h"
#include <iostream>



void public_shared_lib_init()
{
	/* initialize private global state variable */
	private_set_global_variable_content("Hello World");
}

int public_shared_lib_function(int param)
{
	/* use private global state variable */
	std::string content = private_get_global_variable_content();
	std::cout << content << std::endl;
	return param + static_cast<int>(content.length());
}