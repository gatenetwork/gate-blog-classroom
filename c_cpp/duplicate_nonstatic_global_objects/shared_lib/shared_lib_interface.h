#ifndef SAMPLE_SHARED_LIB1_H_INCLUDED
#define SAMPLE_SHARED_LIB1_H_INCLUDED

#if defined(_WINDOWS) 
#   if defined(shared_lib_EXPORTS)
#       define EXPORT_API __declspec(dllexport)
#   else
#       define EXPORT_API __declspec(dllimport)
#   endif
#else
#   define EXPORT_API
#endif

#ifdef __cplusplus
extern "C" {
#endif


EXPORT_API void public_shared_lib_init();

EXPORT_API int public_shared_lib_function(int param);


#ifdef __cplusplus
}
#endif


#endif
