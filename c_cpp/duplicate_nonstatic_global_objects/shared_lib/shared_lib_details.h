#ifndef SHARED_LIB_DETAILS_H_INCLUDED
#define SHARED_LIB_DETAILS_H_INCLUDED

#include <string>

void private_set_global_variable_content(char const* text);

int private_get_global_variable_length();

char const* private_get_global_variable_content();

#endif