
if(USE_STATIC_FOR_PRIVATE_VARS)
  add_definitions(-DUSE_STATIC_FOR_PRIVATE_VARS=1)
endif()

add_library(shared_lib SHARED
    shared_lib_interface.h
    shared_lib_impl.cpp

    shared_lib_details.h
    shared_lib_details.cpp
)

