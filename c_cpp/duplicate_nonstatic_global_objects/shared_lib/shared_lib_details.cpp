#include "shared_lib/shared_lib_details.h"

#if defined(USE_STATIC_FOR_PRIVATE_VARS)
#	define PRIVATE static
#else
#	define PRIVATE
#endif

/* Here is our private global state variable */
PRIVATE std::string private_global_variable("Uninitialized state");


/* modify global state variable */
void private_set_global_variable_content(char const* text)
{
	private_global_variable = text;
}


/* read global state variable */
char const* private_get_global_variable_content()
{
	return private_global_variable.c_str();
}

