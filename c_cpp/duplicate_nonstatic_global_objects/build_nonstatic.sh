#!/bin/sh
export BUILD_FOLDER="./build_nonstatic"
rm -rf $BUILD_FOLDER
mkdir -p $BUILD_FOLDER
cmake -B $BUILD_FOLDER
cmake --build $BUILD_FOLDER
