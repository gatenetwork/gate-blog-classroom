#include "sample_object.h"
#include <stdlib.h>

static void FUNC_ABI sample_object_release(sample_object_t* self)
{
    free(self);
}

static void FUNC_ABI sample_object_set_member(sample_object_t* self, int value)
{
    self->private_member = value;
}

static int FUNC_ABI sample_object_get_member(sample_object_t* self, int* ptr_out_result)
{
	*ptr_out_result = self->private_member;
	return self->private_member;
}
static int FUNC_ABI sample_object_combine(sample_object_t* self, int multiply_member, int add_member, int* ptr_out_result)
{
    int result = self->private_member * multiply_member + add_member;
	*ptr_out_result = result;
	return result;
}

static sample_object_vtbl_t const sample_object_vtbl = 
{
	&sample_object_release,
	&sample_object_set_member,
	&sample_object_get_member,
	&sample_object_combine
};

sample_object_t* create_sample_object(int init_value)
{
	sample_object_t* ptr_sample = (sample_object_t*)malloc(sizeof(sample_object_t));
	if(ptr_sample)
	{
		ptr_sample->vtbl = &sample_object_vtbl;
		ptr_sample->private_member = init_value;
	}
	return ptr_sample;
}

