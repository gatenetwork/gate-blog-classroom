#ifndef VTABLE_SAMPLE_OBJECT_H_INCLUDED
#define VTABLE_SAMPLE_OBJECT_H_INCLUDED

#if defined(__GNUC__)
//#	define FUNC_ABI __attribute__((__cdecl__))
#   define FUNC_ABI
#   if defined(SAMPLE_LIB_EXPORTS)
#       define DLL __attribute__((visibility("default")))
#   else
#       define DLL
#   endif
#elif defined(_MSC_VER)
#   define FUNC_ABI __cdecl
#   if defined(SAMPLE_LIB_EXPORTS)
#       define DLL __declspec(dllexport)
#   else
#       define DLL __declspec(dllimport)
#   endif
#else
#   define DLL
#   define LIBEXPORT
#endif


#if defined(__cplusplus)

class sample_object
{
public:
    virtual void FUNC_ABI release() = 0;
    virtual void FUNC_ABI set_member(int value) = 0;
    virtual int  FUNC_ABI get_member(int& ref_out_result) = 0;
    virtual int  FUNC_ABI combine(int multiply_member, int add_member, int& ref_out_result) = 0;
};

typedef class sample_object sample_object_t;

#else

typedef struct sample_object sample_object_t;

typedef struct
{
    void (FUNC_ABI *release)    (sample_object_t* self);
    void (FUNC_ABI *set_member) (sample_object_t* self, int value);
    int  (FUNC_ABI *get_member) (sample_object_t* self, int* ptr_out_result);
    int  (FUNC_ABI *combine)    (sample_object_t* self, int multiply_member, int add_member, int* ptr_out_result);
} sample_object_vtbl_t;

struct sample_object
{
    sample_object_vtbl_t const* vtbl;

    int private_member;
};

#endif


#if defined(__cplusplus)
extern "C"
#endif

DLL sample_object_t* FUNC_ABI create_sample_object(int init_value);


#endif
