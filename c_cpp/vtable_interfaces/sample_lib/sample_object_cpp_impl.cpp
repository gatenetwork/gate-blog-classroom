#include "sample_object.h"

class sample_object_impl : public sample_object
{
private:
    int private_member;

public:
    sample_object_impl(int init_value)
    : private_member(init_value)
    {
    }

    virtual void FUNC_ABI release()
    {
        delete this;
    }

    virtual void FUNC_ABI set_member(int value)
    {
        this->private_member = value;
    }

    virtual int FUNC_ABI get_member(int& ref_out_result)
    {
        ref_out_result = this->private_member;
        return this->private_member;
    }

    int FUNC_ABI combine(int multiply_member, int add_member, int& ref_out_result)
    {
        int result = this->private_member * multiply_member + add_member;
        ref_out_result = result;
        return result;
    }
};


sample_object_t* create_sample_object(int init_value)
{
    return new sample_object_impl(init_value);
}