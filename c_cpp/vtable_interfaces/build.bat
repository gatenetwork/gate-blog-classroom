@ECHO OFF
SET BUILD_FOLDER="build"
del /S /Q %BUILD_FOLDER% >NUL
mkdir %BUILD_FOLDER% >NUL
cmake -B %BUILD_FOLDER%
cmake --build %BUILD_FOLDER% --config Release
build\output\Release\c_app_uses_c_lib
build\output\Release\c_app_uses_cpp_lib
build\output\Release\cpp_app_uses_c_lib
build\output\Release\cpp_app_uses_cpp_lib
