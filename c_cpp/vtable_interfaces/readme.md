# V-Table memory layout for C and C++

Microsoft's COM defines a binary standard for objects and their virtual 
method tables. An object's interface can be defined in C++ or be put
together with macros as C `struct`s and are interchangeable when accessed.

While Microsoft uses a special calling convention `STDCALL` to make it work,
it looks like it's also possible to apply this procedure with the native 
C calling convention `__cdecl` on current platforms.

## Defining V-Tables and objects in C

V-Tables are just `struct`s containing method function pointers.
To provide an object oriented syntax, the first argument of a method is the
`this` pointer of the object the interface is accessing.

```c
typedef struct my_object my_object_t;
typedef struct 
{
  void (*method1)(my_object_t* self, int param);
  int  (*method2)(my_object_t* self, char const* param1, int param2);
} my_object_vtbl_t;

struct my_object 
{
  my_object_vtbl_t* vtbl;
};
```

## C++ pure virtual classes generate compatible V-Tables

A pure virtual class generates the same basic structure like the 
C example from above. The resulting `my_object` class contains
one defined pointer to a table of two methods.

```cpp
typedef class my_object my_object_t;
class my_object
{
  virtual void method1(int param) = 0;
  virtual int  method2(char const* param1, int param2) = 0;
};
```

## Testing C and C++ V-Table based object interface usage

The provided example codes generate two libraries. 
One defines a C based implementation of an object interface, 
the other one defines it by C++ syntax.

4 executables will be generated, 2 in plain C, 2 in C++ that
link against the C or C++ library and access methods of the
created sample object.

- C Apps
  - `c_app_uses_c_lib`
    - This is a pure C application that links against the pure C library `sample_c_lib`
  - `c_app_uses_cpp_lib`
    - This is the same pure C app but now links against the C++ library `sample_cpp_lib`
- C++ Apps
  - `cpp_app_uses_cpp_lib`
    - This is a C++ application that links the C++ library `sample_cpp_lib` directly.
  - `cpp_app_uses_c_lib`
    - This is the same C++ application but now links against the C library `sample_c_lib`.

## Result evaluation

Executing the samples will perform some methods invocations with parameters 
and expect predefined results.  
In case of ABI incompatibilities between C and C++ object interfaces, we would
see crashes or invalid calculation results.

If all tests are successfull a simple `OK` is printed to `STDOUT`.
