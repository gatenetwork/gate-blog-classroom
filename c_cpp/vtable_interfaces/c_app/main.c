#include "sample_lib/sample_object.h"

#include <stdio.h>
#include <stdlib.h>

static void check(int expected_condition, char const* path, int line)
{
	if(!expected_condition)
	{
		printf("ERROR in: %s (%d)\n", path, line);
		exit(1);
	}
}

#define CHECK(expected_condition) check(expected_condition, __FILE__, __LINE__)

int main()
{
	int init_value = 24;
	int setup_value = 42;
	int multiplier = 2;
	int adder = 5;
	int expected_result = setup_value * multiplier + adder;
	int var1 = 0;
	int var2 = 0;

	sample_object_t* ptr_sample = create_sample_object(init_value);
	CHECK(ptr_sample != NULL);

	var2 = ptr_sample->vtbl->get_member(ptr_sample, &var1);
	CHECK(var1 == init_value);
	CHECK(var2 == init_value);

	ptr_sample->vtbl->set_member(ptr_sample, setup_value);
	var2 = ptr_sample->vtbl->get_member(ptr_sample, &var1);
	CHECK(var1 == setup_value);
	CHECK(var2 == setup_value);

	var2 = ptr_sample->vtbl->combine(ptr_sample, multiplier, adder, &var1);
	CHECK(var1 == expected_result);
	CHECK(var2 == expected_result);

	ptr_sample->vtbl->release(ptr_sample);

	printf("OK\n");
	return 0;
}