#!/bin/sh

BUILD_FOLDER="build"

echo $BUILD_FOLDER
rm -rf $BUILD_FOLDER
mkdir -p $BUILD_FOLDER
cd $BUILD_FOLDER
cmake ..
make
./output/c_app_uses_c_lib
./output/c_app_uses_cpp_lib
./output/cpp_app_uses_c_lib
./output/cpp_app_uses_cpp_lib
cd ..