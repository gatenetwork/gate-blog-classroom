#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <atomic>
#include <stdexcept>
#include <iomanip>

class Loop_worker
{
public:
    Loop_worker();
    ~Loop_worker();

    void start(std::function<void()> code, std::chrono::milliseconds interval);
    bool stop();

private:
    std::thread worker_thread;
    std::chrono::milliseconds worker_interval;
    std::function<void()> worker_code;
    std::mutex stop_mutex;
    std::condition_variable stop_signal;
    std::atomic_bool is_started;

    void thread_function();
};

Loop_worker::Loop_worker()
{
    is_started.store(false);
}

Loop_worker::~Loop_worker()
{
    stop();
}

void Loop_worker::start(std::function<void()> code, std::chrono::milliseconds interval)
{
    bool expected_state = false;
    if(!is_started.compare_exchange_strong(expected_state, true))
    {
        throw std::runtime_error("Invalid state");
    }
    worker_interval = interval;
    worker_code = code;
    try
    {
        worker_thread = std::thread(&Loop_worker::thread_function, this);
    }
    catch(...)
    {
        is_started.store(false);
        throw;
    }
}

bool Loop_worker::stop()
{
    bool expected_state = true;
    if(!is_started.compare_exchange_strong(expected_state, false))
    {
        return false;   // invalid state
    }
    stop_signal.notify_all();
    if(worker_thread.joinable())
    {
        worker_thread.join();
    }    
    return true;
}

void Loop_worker::thread_function()
{
    bool is_stopped = false;
    auto stopped_state = [this]() { return !is_started.load(); };

    do
    {
        auto next_timeout = std::chrono::steady_clock::now() + worker_interval;

        try
        {
            worker_code();
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << std::endl;
        }

        std::unique_lock<std::mutex> stop_lock(stop_mutex);
        is_stopped = stop_signal.wait_until(stop_lock, next_timeout, stopped_state);
    } while(!is_stopped);
}

int main()
{
    //std::ios::sync_with_stdio(false);

    auto background_code = []() {
        auto now = std::chrono::system_clock::now();
        auto timestamp = std::chrono::system_clock::to_time_t(now);
        auto time = std::localtime(&timestamp);
        static int counter = 0;
        std::cout 
            << "                    \r"
            << std::put_time(time, "%Y-%m-%d %H:%M:%S") 
            << "  [interval #" << ++counter << "]";
        std::cout.flush();
    };

    std::cout << std::endl << "Press any key to exit program" << std::endl << std::endl;

    Loop_worker worker;
    worker.start(background_code, std::chrono::seconds(1));

    std::getchar();

    worker.stop();

    std::cout << std::endl;

    return 0;
}
