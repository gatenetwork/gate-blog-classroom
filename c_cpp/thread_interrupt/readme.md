# interruptable thread loop sample

This example shows a worker thread calling a routine in a specific 
interval in a loop. Its waiting routine can be interrupted by 
signaling a condition variable.

## Prerequisites

- C++ 11
- CMake v3.0+
- GCC 4.8+
- MSVC 2012+


## Demo setup

Invoke `build.sh` or `build.bat` to run the build process.
