#!/bin/sh
export BUILD_DIR=./build
export SOURCE_DIR=..
if [ -d "$BUILD_DIR" ]; then rm -r $BUILD_DIR; fi
mkdir -p $BUILD_DIR
cd $BUILD_DIR
cmake -DCMAKE_BUILD_TYPE=Release $SOURCE_DIR
make
cd $SOURCE_DIR
