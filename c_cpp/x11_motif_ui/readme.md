# X11 motif sample

Motif is a UI widget library on top of X Toolkit of X11 X Windowing System.

## Motif UIs

X-Toolkit and Motif UIs consist of

- Primitive widgets like Labels, Buttons and Textfields.
- Composite widgets that contain and arrange other widgets for size and positions.
- Toplevel widgets covering an application window frame.

Each widget can be configured with specific values that define its
contents (e.g. a text label) or its display properties 
(e.g. position-bindings, color, ...)

UI elements can raise events on their activation or manipulation 
which can be handled by registered event callbacks to react on
user and system interaction.

UI Events are handled by an event loop which runs continously
until an "Exit-Flag" is set.

## Demo App

The `hello_motif` app creates a top level window containing 
3 user controls:
- a label showing a message and the current date.
- an update button to refresh the label's content with a new date value.
- an exit button to close the application.
 
 The window hosts a `XmForm` widget that controls the layout
 of our 3 user controls:
 - The update button is attached to the top of the window.
 - The exit buttom is attached to the bottom of the window.
 - The label is attached between both buttons.
 - Resizing the window will also resize all contained widgets.
