#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <Xm/XmAll.h>

XtAppContext app_context;
Display* display;
Widget main_window;
Widget main_window_form;
Widget label;
Widget button_update;
Widget button_exit;


static void fatal_exit(char const* message)
{
    fprintf(stderr, "%s\n", message);
    abort();
}

static void set_widget_label(Widget w, char const* text)
{
    char textbuffer[4096];
     XmString xmstr;

    sprintf(textbuffer, "%s", text);
    xmstr = XmStringCreateLocalized(textbuffer);
    if(!xmstr)
    {
        fatal_exit("XmStringCreateLocalized() failed");
    }
    XtVaSetValues(w, XmNlabelString, xmstr, NULL);
    XmStringFree(xmstr);
}

static void print_message_and_current_date()
{
    time_t time_stamp = time(NULL);
    struct tm* ptr_date;
    char buffer[1024];
    ptr_date = localtime(&time_stamp);
    if(!ptr_date)
    {
        fatal_exit("localtime() failed");
    }
    sprintf(buffer, "%s\n\nCurrent Time: %04d-%02d-%02d %02d:%02d:%02d", 
        "Hello World",
        (ptr_date->tm_year + 1900),
        (ptr_date->tm_mon + 1),
        (ptr_date->tm_mday),
        ptr_date->tm_hour,
        ptr_date->tm_min,
        ptr_date->tm_sec
        );
    set_widget_label(label, buffer);
}

static void button_update_pushed(Widget widget, XtPointer client_data, XtPointer call_data)
{
    print_message_and_current_date();
}

static void button_exit_pushed(Widget widget, XtPointer client_data, XtPointer call_data)
{
    XtAppSetExitFlag(app_context);
}

static char app_name[] = "hello_motif";
static char app_class_name[] = "motif_demo_apps";

static void init_environment(int argc, char** argv)
{
    XtToolkitInitialize();

    app_context = XtCreateApplicationContext();
    if(!app_context)
    {
        fatal_exit("XtCreateApplicationContext() failed");
    }

    display = XtOpenDisplay(app_context, NULL, app_name, app_class_name, NULL, 0, &argc, argv);
    if(!display)
    {
        fatal_exit("XtOpenDisplay() failed");
    }
}

static void create_ui()
{
    /* create main application window with dimension 320 x 240*/
    main_window = XtAppCreateShell(NULL, NULL, applicationShellWidgetClass, display, NULL, 0);
    if(!main_window)
    {
        fatal_exit("XtAppCreateShell(applicationShellWidgetClass) failed");
    }
    XtVaSetValues(main_window, XtNx, 0, XtNy, 0, XtNwidth, 320, XtNheight, 240, NULL);


    /* create a form composite widget that manages child widgets */
    main_window_form = XtCreateWidget(NULL, xmFormWidgetClass, main_window, NULL, 0);
    if(!main_window_form)
    {
        fatal_exit("XtCreateWidget(xmFormWidgetClass) failed");
    }
    XtManageChild(main_window_form);


    /* create a button on attached to top of main window */
    button_update = XtCreateWidget(NULL, xmPushButtonWidgetClass, main_window_form, NULL, 0);
    if(!button_update)
    {
        fatal_exit("XtCreateWidget(xmPushButtonWidgetClass) failed");
    }
    XtVaSetValues(button_update, 
        XmNtopAttachment, XmATTACH_FORM,
        XmNleftAttachment, XmATTACH_FORM,
        XmNrightAttachment, XmATTACH_FORM,
        NULL);
    set_widget_label(button_update, "Update");
    XtAddCallback(button_update, XmNactivateCallback, &button_update_pushed, NULL);
    XtManageChild(button_update);


    /* create a button attached to bottom of main window */
    button_exit = XtCreateWidget(NULL, xmPushButtonWidgetClass, main_window_form, NULL, 0);
    if(!button_exit)
    {
        fatal_exit("XtCreateWidget(xmPushButtonWidgetClass) failed");
    }
    XtVaSetValues(button_exit, 
        XmNbottomAttachment, XmATTACH_FORM,
        XmNleftAttachment, XmATTACH_FORM,
        XmNrightAttachment, XmATTACH_FORM,
        NULL);
    set_widget_label(button_exit, "Exit");
    XtAddCallback(button_exit, XmNactivateCallback, &button_exit_pushed, NULL);
    XtManageChild(button_exit);


    /* create a label widget attached to top and bottom pushbuttons */
    label = XtCreateWidget(NULL, xmLabelWidgetClass, main_window_form, NULL, 0);
    if(!label)
    {
        fatal_exit("XtCreateWidget(xmLabelWidgetClass) failed");
    }
    XtVaSetValues(label, 
        XmNleftAttachment, XmATTACH_FORM,
        XmNrightAttachment, XmATTACH_FORM,
        XmNtopAttachment, XmATTACH_WIDGET,
        XmNtopWidget , button_update,
        XmNbottomAttachment, XmATTACH_WIDGET,
        XmNbottomWidget , button_exit,
        NULL);
    XtManageChild(label);

    /* make main window visible */
    XtRealizeWidget(main_window);

    print_message_and_current_date();
}

static void destroy_ui()
{
    XtDestroyWidget(button_exit);
    XtDestroyWidget(button_update);
    XtDestroyWidget(label);
    XtDestroyWidget(main_window_form);
    XtDestroyWidget(main_window);
}

static void run_ui_loop()
{
    XEvent xevt;

	while(!XtAppGetExitFlag(app_context))
	{
		XtAppNextEvent(app_context, &xevt);
		XtDispatchEvent(&xevt);
	}

    /* or just run: 
        XtAppMainLoop(app_context);
    */
}

static void uninit_environment()
{
    XtCloseDisplay(display);
    XtDestroyApplicationContext(app_context);
}


int main(int argc, char* argv[])
{
    init_environment(argc, argv);

    create_ui();

    run_ui_loop();

    uninit_environment();
}
