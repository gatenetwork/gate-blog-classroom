# Windows XP/Vista/7/8 auto-login password reader

This small console program reads the stored `DefaultPassword` Fields
from the LSA-POLICY Object and alternatively from the registry.

Windows Tools like `netplwiz` (can be started with `control userpasswords2`)
allow to disable the login procedure on Windows startup and to set a 
default user with a default password to login in automatically without
user interaction.

## Prerequisites

- CMake v3.0+
- Microsoft Visual Studio 2005 or newer.

## Build script

Invoke `build_with_cmake.bat` to run `cmake` to build to provided source 
codes. The resulting executable named `win32_default_password_reader.exe`
will be placed in `/bin/Release` output folder.

## Program

Open the command line (e.g. `cmd.exe` or `powershell.exe`) with administrative
rights and run `win32_default_password_reader.exe`.  
If passwords can be extracted from LSA or from the Windows registry,
they are printed onto the console.  
Otherwise an error code is printed or simply `No password` if the requested
fields are empty.
