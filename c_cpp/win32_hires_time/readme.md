# Classic Windows high-resolution timer sample

Windows time APIs work with an accuracy of about 15 milliseconds.
While Windows 10 has introduced a smaller update interval of about 1 or 2
milliseconds, previous Windows versions cannot deliver the current time
in such an accurate way.

This program demonstrates a common workaround to manually update timestamps
to increase accuracy.  
It is based on public source codes like 
[those from MSDN magazine](https://learn.microsoft.com/en-us/archive/msdn-magazine/2004/march/implementing-a-high-resolution-time-provider-for-windows)

## Prerequisites

- CMake v3.0+
- Microsoft Visual Studio 2005 or newer.
- Windows version older than Windows 10 (Win 10 did already solve it internally).

## Build script

Invoke `build_with_cmake.bat` to run `cmake` to build to provided source 
codes. The resulting executable named `win32_hires_time.exe`
will be placed in `/bin/Release` output folder.

## Program

The program first initializes high-resolution timers to get more update
and wake-up ticks from the system.
It uses classic API `GetSystemTimeAsFileTime()` to demonstrate,
that accuracy is still low, the same timestamp is repeated multiple times.  
Afterwards it runs the same demonstration code with patched time functions
to add performance counter values to the last received system time.
It shows a better accuracy of about +/- 2 milliseconds instead of up to 
-15 milliseconds.

Your should redirect console output to a file with  
`win32_hires_time >output.txt`  
to see all recorded timestamps.
There should be 1000 timestamps in one demo-run, and each time-request is
delayed by `Sleep(1)` which in total becomes about 1 to 3 milliseconds 
depending on current system load and CPU power.

## Known issues

- Retrieved timestamps can jump back in time.
  This depends on the `Sleep(1)` between calls that brings
  retrieving system time and performance counter values 
  a little bit out of sync.  
  A correct implementation would run this code in a high priority
  thread without sleeps in between to ensure proper synchronization.  
  However, this is just a small demo ;)