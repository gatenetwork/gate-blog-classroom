#include <stdio.h>

#include "windows.h"

#pragma comment(lib, "winmm.lib")

/* pointer to a time-retrieval implementation function */
typedef void(*get_win32_time_function_t)(FILETIME* ptr_time);

/* classic way of retrieving current time */
void classic_get_win32_current_time(FILETIME* ptr_time)
{
	GetSystemTimeAsFileTime(ptr_time);
}


/* global variables to store current high resolution
   timestamp and counter frequency */
static LARGE_INTEGER hires_timestamp;
static LARGE_INTEGER hires_frequency;

static void hires_update_current_time()
{
	static LARGE_INTEGER ref_timestamp;
	static LARGE_INTEGER ref_counter;

	FILETIME current_time;
	LARGE_INTEGER timestamp;
	LARGE_INTEGER counter;
	LARGE_INTEGER elapsed_ticks;
	LARGE_INTEGER update_time;

	/* retrieve current system time and perforance counter */
	GetSystemTimeAsFileTime(&current_time);
	QueryPerformanceCounter(&counter);

	/* generate INT64 timestamp */
	timestamp.HighPart = (LONG)current_time.dwHighDateTime;
	timestamp.LowPart = current_time.dwLowDateTime;		

	if(timestamp.QuadPart != ref_timestamp.QuadPart)
	{
		/*	system clock was updated, 
			store reference timestamp and counter */
		ref_timestamp.QuadPart = timestamp.QuadPart;
		ref_counter.QuadPart = counter.QuadPart;
	}
	else
	{
		if(counter.QuadPart == ref_counter.QuadPart)
		{
			/*	no time and counter update since last
				invocation -> nothing to do */
			return;
		}
	}

	/* get elapsed counter ticks since last update */
	elapsed_ticks.QuadPart = counter.QuadPart - ref_counter.QuadPart;

	/* convert counter ticks to FILETIME units */
	update_time.QuadPart = (ULONGLONG)(10000000.0 *
		((double)elapsed_ticks.QuadPart / (double)hires_frequency.QuadPart));

	/* store current hi-res timestamp */
	hires_timestamp.QuadPart = ref_timestamp.QuadPart + update_time.QuadPart;
}

static void hires_init()
{
	/* increase system timer accuracy to 1ms */
	timeBeginPeriod(1);

	/* retrieve high resolution frequency */
	QueryPerformanceFrequency(&hires_frequency);
}

static void hires_uninit()
{
	/* decrease system timer accuracy back to default */
	timeEndPeriod(1);
}

/* high-resolution filetime */
void hires_get_win32_current_time(FILETIME* ptr_time)
{
	hires_update_current_time();
	ptr_time->dwHighDateTime = hires_timestamp.HighPart;
	ptr_time->dwLowDateTime = hires_timestamp.LowPart;
}

void demo_print_current_time(get_win32_time_function_t get_time_func)
{
#define MAX_TIMESTAMPS 1000
	FILETIME stored_timestamps[MAX_TIMESTAMPS];
	SYSTEMTIME sys_time;
	int cnt;

	/* store all timestamps in memory */
	/* direct printing would cause too much noise and delay */
	for(cnt = 0; cnt != MAX_TIMESTAMPS; ++cnt)
	{
		get_time_func(&stored_timestamps[cnt]);
		Sleep(1);	/* sleep for 1 ms */
	}

	/* now print stored timestamps */
	for(cnt = 0; cnt != MAX_TIMESTAMPS; ++cnt)
	{
		FileTimeToSystemTime(&stored_timestamps[cnt], &sys_time);
		printf("%02d:%02d:%02d.%03d\n", 
			(int)sys_time.wHour, 
			(int)sys_time.wMinute, 
			(int)sys_time.wSecond, 
			(int)sys_time.wMilliseconds);
	}
}


int main()
{
	/* setup high-resolution support */
	hires_init();

	printf("Retrieving classic system time:\n");
	demo_print_current_time(&classic_get_win32_current_time);

	printf("----------------------------------------\n");
	
	printf("Retrieving high resolution time:\n");
	demo_print_current_time(&hires_get_win32_current_time);

	/* remove high-resolution support */
	hires_uninit();

	return 0;
}
