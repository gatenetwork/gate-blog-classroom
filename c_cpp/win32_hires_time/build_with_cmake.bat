SET INIT_DIR=%~dp0
SET BUILD_DIR=%INIT_DIR%\build
RMDIR /s /q %BUILD_DIR%
MKDIR %BUILD_DIR%
CHDIR %BUILD_DIR%
cmake %INIT_DIR%
cmake --build %BUILD_DIR% --config Release
pause