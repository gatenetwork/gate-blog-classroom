#include <stdio.h>


static int foo_1(int a)
{
	printf("foo_1 called\n");
	return a * -1;
}

static int foo_2(int a, int b)
{
	printf("foo_2 called\n");
	return a * b;
}

static int foo_3(int a, int b, int c)
{
	printf("foo_3 called\n");
	return a + b + c;
}


#define COUNT_ARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, N,...) N
#define COUNT_ARGS_HELPER(tuple) COUNT_ARGS_IMPL tuple
#define COUNT_ARGS(...) COUNT_ARGS_HELPER((__VA_ARGS__, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1))

#define CONCAT(a, b) a ## b
#define BUILD_FUNCTION(func_prefix, arg_count) CONCAT(func_prefix, arg_count)
#define CALL_OVERLOADED(func_prefix, args) BUILD_FUNCTION(func_prefix, COUNT_ARGS args)  args

//#define foo(...) CALL_OVERLOADED(foo_, COUNT_ARGS(__VA_ARGS__)) ( __VA_ARGS__ )
#define foo(...) CALL_OVERLOADED(foo_, (__VA_ARGS__))

int main()
{
	int x = foo(10);
	int y = foo(20, 30);
	int z = foo(40, 50, 60);

	printf("x=%d, y=%d, z=%d", x, y, z);
	return 0;
}
