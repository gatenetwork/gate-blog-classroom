#!/bin/sh
BUILD_FOLDER="./build"
rm -rf $BUILD_FOLDER
mkdir -p $BUILD_FOLDER
cd $BUILD_FOLDER
cmake ..
cmake --build .
cd ..
