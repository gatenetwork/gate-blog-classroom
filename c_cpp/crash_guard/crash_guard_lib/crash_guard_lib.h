#ifndef CRASH_GUARD_LIB_H_INCLUDED
#define CRASH_GUARD_LIB_H_INCLUDED

typedef void*(*crash_guard_entry_point)(void* param);

#define CRASH_GUARD_SUCCEEDED 0
#define CRASH_GUARD_FAILED_INTERNAL 1
#define CRASH_GUARD_FAILED_UNKNOWN 2
#define CRASH_GUARD_FAILED_MEMFAULT 3
#define CRASH_GUARD_FAILED_CODEFAULT 4
#define CRASH_GUARD_FAILED_ARITHMETIC 5
#define CRASH_GUARD_FAILED_SYSTEM 6
#define CRASH_GUARD_FAILED_EXCEPTION 7

#ifdef __cplusplus 
extern "C" {
#endif

int crash_guard_init();

int crash_guard_invoke(crash_guard_entry_point entry_point, void* param, void** ptr_return_value);

#ifdef __cplusplus 
}
#endif


#endif /* CRASH_GUARD_LIB_H_INCLUDED */
