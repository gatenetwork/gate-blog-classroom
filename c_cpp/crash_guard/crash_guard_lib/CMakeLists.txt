
add_library(crash_guard_lib STATIC
  crash_guard_lib.h
  crash_guard_lib.hpp
  crash_guard_lib_posix.c
  crash_guard_lib_windows.c
)
