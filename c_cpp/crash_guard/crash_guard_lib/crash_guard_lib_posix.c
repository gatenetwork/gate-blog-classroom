#include "crash_guard_lib.h"

#if !defined(_MSC_VER)

/* POSIX signal implementation */

#include <pthread.h>
#include <setjmp.h>
#include <signal.h>

#define CRASH_GUARD_SIGJMP_SUPPORTED 1

#if !defined(CRASH_GUARD_SIGJMP_SUPPORTED)

/* workaround for systems without sigsetjmp() and siglongjmp()
   we just use classic setjmp() and longjmp()
*/

#   define jmp_buf sigjmp_buf

static int sigsetjmp(sigjmp_buf env, int savemask)
{
    return setjmp(env);
}

static void siglongjmp(sigjmp_buf env, int val)
{
    longjmp(env, val);
}

#endif

static pthread_key_t global_crash_context_key;

typedef struct
{
    sigjmp_buf anchor;
} crash_guard_context_t;


/* define a global mapping from native signal numbers to CRASH_GUARD result codes */

typedef struct
{
    int signum;
    int crash_guard_code;
} signal_mapping_t;

static signal_mapping_t const signal_mappings[] =
{
#ifdef SIGSYS
    { SIGSYS, CRASH_GUARD_FAILED_SYSTEM},       /* system call has failed */
#endif
#ifdef SIGBUS
    { SIGBUS, CRASH_GUARD_FAILED_MEMFAULT},     /* memory bus access error */
#endif
#ifdef SIGABRT
    { SIGABRT, CRASH_GUARD_FAILED_CODEFAULT },  /* abort() was called */
#endif
    { SIGILL, CRASH_GUARD_FAILED_CODEFAULT},    /* illegal cpu instruction */
    { SIGFPE, CRASH_GUARD_FAILED_ARITHMETIC },  /* division by zero or some other calculation error */
    { SIGSEGV, CRASH_GUARD_FAILED_MEMFAULT }    /* invalid memory access */
};

static size_t const signal_mappings_count = sizeof(signal_mappings) / sizeof(signal_mappings[0]);


/* global signal handler that resolves the signal and jumps back to the last registered anchor */
static void crash_guard_signal_handler(int signum)
{
    crash_guard_context_t* ptr_anchor = (crash_guard_context_t*)pthread_getspecific(global_crash_context_key);
    int crash_guard_code = CRASH_GUARD_FAILED_UNKNOWN;
    size_t index;    

    if(ptr_anchor)
    {
        /* if local thread has set an anchor jump-buffer, use it */
        for(index = 0; index != signal_mappings_count; ++index)
        {
            if(signal_mappings[index].signum == signum)
            {
                /* map signal-number to crash-guard code */
                crash_guard_code = signal_mappings[index].crash_guard_code;
                break;
            }
        }
        /* jump back to anchor */
        siglongjmp(ptr_anchor->anchor, crash_guard_code);
    }
    else
    {
        /* otherwise return from signal handler without any reaction */
    }
}




/* initialize crash guard by registering global signal handlers and thread-local master key */
int crash_guard_init()
{
    size_t index;

    if(!global_crash_context_key)
    {
        if(0 != pthread_key_create(&global_crash_context_key, NULL))
        {
            return CRASH_GUARD_FAILED_INTERNAL;
        }
    }

    for(index = 0; index != signal_mappings_count; ++index)
    {
        signal(signal_mappings[index].signum, &crash_guard_signal_handler);
    }

    return CRASH_GUARD_SUCCEEDED;
}


int crash_guard_invoke(crash_guard_entry_point entry_point, void* param, void** ptr_return_value)
{
    int ret;
    crash_guard_context_t context;
    crash_guard_context_t* ptr_old_context = NULL;
    void* entry_point_return_value;
    size_t index;
    sigset_t new_set;
    sigset_t old_set;

    do
    {
        /* retrieve previously registered context to be restored on exit */
        ptr_old_context = (crash_guard_context_t*)pthread_getspecific(global_crash_context_key);

        /* register local context buffer with the local thread to be used as an anchor for non-local jumps */ 
        if(0 != pthread_setspecific(global_crash_context_key, &context))
        {
            ret = CRASH_GUARD_FAILED_INTERNAL;
            break;
        }

        sigemptyset(&new_set);
        for(index = 0; index != signal_mappings_count; ++index)
        {
            sigaddset(&new_set, signal_mappings[index].signum);
        }

        /* unblock all supported signals */
        if(0 != pthread_sigmask(SIG_UNBLOCK, &new_set, &old_set))
        {
            ret = CRASH_GUARD_FAILED_INTERNAL;
            break;
        }

        /* set anchor to return here in case of a signal */
        ret = sigsetjmp(context.anchor, 1);

        if(ret == 0)
        {
            /* this branch is executed when */
            if(entry_point)
            {
                /* invoke guarded entry-point function */
                entry_point_return_value = entry_point(param);
                if(ptr_return_value)
                {
                    /* hand over entry-point's return value to caller */
                    *ptr_return_value = entry_point_return_value;
                }
            }
            ret = CRASH_GUARD_SUCCEEDED;
        }
        else
        {
            /* this branch is executed, if the signal handler has jumped back */
            /* nothing to do, "ret" is set by the signal handler */
        }

        /* restore the list of blocked/unblocked signals */
        pthread_sigmask(SIG_SETMASK, &old_set, NULL);

    } while (0);
    
    /* restore previously registered context */
    pthread_setspecific(global_crash_context_key, ptr_old_context);
    return ret;
}

#endif
