#ifndef CRASH_GUARD_LIB_HPP_INCLUDED
#define CRASH_GUARD_LIB_HPP_INCLUDED

#include "crash_guard_lib/crash_guard_lib.h"

#include <cstdlib>
#include <functional>
#include <exception>

struct CrashGuardInternalError : public std::exception
{
	virtual char const* what() const throw()
	{
		return "CrashGuardInternalError";
	}
};

struct CrashGuardUnknownError : public std::exception
{
	virtual char const* what() const throw()
	{
		return "CrashGuardUnknownError";
	}
};

struct CrashGuardMemFault : public std::exception
{
	virtual char const* what() const throw()
	{
		return "CrashGuardMemFault";
	}
};
struct CrashGuardCodeFault : public std::exception
{
	virtual char const* what() const throw()
	{
		return "CrashGuardCodeFault";
	}
};
struct CrashGuardArithmeticFault : public std::exception
{
	virtual char const* what() const throw()
	{
		return "CrashGuardArithmeticFault";
	}
};
struct CrashGuardSystemFault : public std::exception
{
	virtual char const* what() const throw()
	{
		return "CrashGuardSystemFault";
	}
};
struct CrashGuardException : public std::exception
{
	virtual char const* what() const throw()
	{
		return "CrashGuardException";
	}
};

class CrashGuard
{
private:
	template<class F, class R>
	struct Dispatcher
	{
	public:

		F	functor;
		R*	ptr_return_value;


		Dispatcher(F functor_arg)
		:	functor(functor_arg),
			ptr_return_value(NULL)
		{
		}

		Dispatcher(F functor_arg, R* ptr_return_arg)
		:	functor(functor_arg),
			ptr_return_value(ptr_return_arg)
		{
		}

		static void* dispatch(void* args)
		{
			Dispatcher* disp = static_cast<Dispatcher*>(args);
			try
			{
				disp->functor();
				return NULL;
			}
			catch(...)
			{
			}
			return reinterpret_cast<void*>(CRASH_GUARD_FAILED_EXCEPTION);
		}

		static void* dispatch_rv(void* args)
		{
			Dispatcher* disp = static_cast<Dispatcher*>(args);
			try
			{
				*disp->ptr_return_value = disp->functor();
				return NULL;
			}
			catch(...)
			{
			}
			return reinterpret_cast<void*>(CRASH_GUARD_FAILED_EXCEPTION);
		}
	};

	static void handle_crash_guard_results(int crash_code, void* cpp_exception)
	{
		switch(crash_code)
		{
		case CRASH_GUARD_SUCCEEDED:
			{
				if(cpp_exception)
				{
					throw CrashGuardException();
				}
				break;
			}
		case CRASH_GUARD_FAILED_INTERNAL:	throw CrashGuardInternalError();
		case CRASH_GUARD_FAILED_UNKNOWN:	throw CrashGuardUnknownError();
		case CRASH_GUARD_FAILED_MEMFAULT:	throw CrashGuardMemFault();
		case CRASH_GUARD_FAILED_CODEFAULT:	throw CrashGuardCodeFault();
		case CRASH_GUARD_FAILED_ARITHMETIC:	throw CrashGuardArithmeticFault();
		case CRASH_GUARD_FAILED_SYSTEM:		throw CrashGuardSystemFault();
		case CRASH_GUARD_FAILED_EXCEPTION:	throw CrashGuardException();
		}
	}

public:

	static void init()
	{
		crash_guard_init();
	}

	template<class F, class R> 
	static void invoke(F functor, R& return_value)
	{
		void* cpp_exception = NULL;
		Dispatcher<F, R> dispatcher(functor, &return_value);
		
		int crash_code = crash_guard_invoke(&Dispatcher<F, R>::dispatch_rv, &dispatcher, &cpp_exception);
		handle_crash_guard_results(crash_code, cpp_exception);
	}

	template<class F>
	static void invoke(F functor)
	{
		void* cpp_exception = NULL;
		Dispatcher<F, void> dispatcher(functor);

		int crash_code = crash_guard_invoke(&Dispatcher<F, void>::dispatch, &dispatcher, &cpp_exception);
		handle_crash_guard_results(crash_code, cpp_exception);
	}

};

#endif /* CRASH_GUARD_LIB_HPP_INCLUDED */
