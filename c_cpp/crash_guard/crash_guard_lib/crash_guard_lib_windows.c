#include "crash_guard_lib.h"

#if defined(_MSC_VER)

/* MSVC Windows structured exception handling (SEH) implementation */

#include <windows.h>

int crash_guard_init()
{
    /* windows SEH does not need special initialization */
    return CRASH_GUARD_SUCCEEDED;
}


static int crash_guard_seh_handler(unsigned int exception_code, struct _EXCEPTION_POINTERS * xcpt_ptrs, int* ptr_error_type)
{
    /* map Win32 exceptions to CRASH_GUARD codes */

    switch(exception_code)
    {
	case EXCEPTION_ACCESS_VIOLATION:
	case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
	case EXCEPTION_GUARD_PAGE:
	case EXCEPTION_IN_PAGE_ERROR: 
	case EXCEPTION_STACK_OVERFLOW:
        {
            /* memory access fault */
            *ptr_error_type = CRASH_GUARD_FAILED_MEMFAULT;
            break;
        }
	case EXCEPTION_ILLEGAL_INSTRUCTION:
	case EXCEPTION_PRIV_INSTRUCTION:
    case EXCEPTION_DATATYPE_MISALIGNMENT:
        {
            /* invalid code or data */
            *ptr_error_type = CRASH_GUARD_FAILED_CODEFAULT;
            break;
        }
    case EXCEPTION_FLT_DENORMAL_OPERAND:
    case EXCEPTION_FLT_DIVIDE_BY_ZERO:
    case EXCEPTION_FLT_INEXACT_RESULT:
    case EXCEPTION_FLT_INVALID_OPERATION:
    case EXCEPTION_FLT_OVERFLOW:
    case EXCEPTION_FLT_STACK_CHECK:
    case EXCEPTION_FLT_UNDERFLOW:
    case EXCEPTION_INT_DIVIDE_BY_ZERO:
    case EXCEPTION_INT_OVERFLOW:
        {
            /* arithmetic error */
            *ptr_error_type = CRASH_GUARD_FAILED_ARITHMETIC;
            break;
        }
	case EXCEPTION_INVALID_DISPOSITION:
    case EXCEPTION_INVALID_HANDLE:
        {
            /* system error */
            *ptr_error_type = CRASH_GUARD_FAILED_SYSTEM;
            break;
        }
	case EXCEPTION_NONCONTINUABLE_EXCEPTION:
    default:
        {
            /* all other exceptions  */
            *ptr_error_type = CRASH_GUARD_FAILED_UNKNOWN;
            break;
        }
    }

    return EXCEPTION_EXECUTE_HANDLER;
}

int crash_guard_invoke(crash_guard_entry_point entry_point, void* param, void** ptr_return_value)
{
    int error_code = 0;
    void* entry_point_return_value;

    __try
    {
        if(entry_point)
        {
            entry_point_return_value = entry_point(param);
            if(ptr_return_value)
            {
                *ptr_return_value = entry_point_return_value;
            }
        }
        return CRASH_GUARD_SUCCEEDED;
    }
    __except(crash_guard_seh_handler(GetExceptionCode(), GetExceptionInformation(), &error_code))
    {
        /* crash_guard_seh_handler() converts the native error code in our generic identifier */
        return error_code;
    }
}

#endif

