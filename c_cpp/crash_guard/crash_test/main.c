#include <stdio.h>
#include <stddef.h>
#include "crash_guard_lib/crash_guard_lib.h"


static int* global_pointer = NULL;

static void set_divisor(int* ptr_divisor)
{
    global_pointer = ptr_divisor;
}

static void* dangerous_division(void* param)
{
    /* use volatile to enforce execution order in release-builds too */
    int volatile result;
    int volatile dividend;
    int volatile divisor;
    void* volatile ret;

    printf("--- entering dangerous_division()\r\n");

    dividend = *(int*)param;
    printf("--- > first parameter accessed: %d\r\n", dividend);
    divisor = *global_pointer;
    printf("--- > second parameter accessed: %d\r\n", divisor);

    result = dividend / divisor;
    printf("--- > division operation completed\r\n");

    ret = (void*)(ptrdiff_t)result;
    printf("--- leaving dangerous_division()\r\n");
    return ret;
}

static void print_crash_result(int result)
{
    switch(result)
    {
    case CRASH_GUARD_SUCCEEDED:
        printf("OK");
        break;
    case CRASH_GUARD_FAILED_INTERNAL:
        printf("Failed: Internal error");
        break;
    case CRASH_GUARD_FAILED_UNKNOWN:
        printf("Failed: Unknown fault");
        break;
    case CRASH_GUARD_FAILED_MEMFAULT:
        printf("Failed: Memory fault");
        break;
    case CRASH_GUARD_FAILED_CODEFAULT:
        printf("Failed: Code fault");
        break;
    case CRASH_GUARD_FAILED_ARITHMETIC:
        printf("Failed: Arithmetic fault");
        break;
    case CRASH_GUARD_FAILED_SYSTEM:
        printf("Failed: System fault");
        break;
    default:
        printf("Unsupported fault code");
        break;
    }
}

void test_without_crash()
{
    int dividend = 12;
    int divisor = 2;
    void* division_result;
    int crash_result;

    printf("Test without crash...\r\n");

    set_divisor(&divisor);

    crash_result = crash_guard_invoke(&dangerous_division, &dividend, &division_result);

    print_crash_result(crash_result);
    printf("\r\n\r\n");
}

void test_with_mem_crash()
{
    int dividend = 12;
    void* division_result;
    int crash_result;

    printf("Test with invalid memory address...\r\n");

    set_divisor(NULL);

    crash_result = crash_guard_invoke(&dangerous_division, &dividend, &division_result);

    print_crash_result(crash_result);
    printf("\r\n\r\n");
}

void test_with_div_crash()
{
    int dividend = 12;
    int divisor = 0;
    void* division_result;
    int crash_result;

    printf("Test with zero divisor...\r\n");

    set_divisor(&divisor);

    crash_result = crash_guard_invoke(&dangerous_division, &dividend, &division_result);

    print_crash_result(crash_result);
    printf("\r\n\r\n");
}


int main()
{
    int result;

    printf("Initializing crash guard... ");
    result = crash_guard_init();
    print_crash_result(result);
    printf("\r\n\r\n");
    if(result != CRASH_GUARD_SUCCEEDED)
    {
        return 1;
    }

    printf(":: Begin of crash_test ::\r\n\r\n");

    test_without_crash();
    test_with_mem_crash();
    test_with_div_crash();

    printf(":: End of crash_test ::\r\n");

    return 0;
}