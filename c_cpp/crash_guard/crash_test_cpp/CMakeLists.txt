set (CMAKE_CXX_STANDARD 11)

add_executable(crash_test_cpp
  main.cpp
)

target_link_libraries(crash_test_cpp
  crash_guard_lib
)

if(UNIX)
  target_link_libraries(crash_test_cpp
    pthread
  )
endif()
