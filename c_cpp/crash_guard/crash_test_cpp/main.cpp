#include "crash_guard_lib/crash_guard_lib.hpp"

#include <iostream>

template<class F>
void run_test(char const* name, F functor)
{
    std::cout << "Run: " << name << " ... ";
    try
    {
        CrashGuard::invoke(functor);
        std::cout << "OK" << std::endl;
    }
    catch(std::exception const& xcpt)
    {
        std::cerr << "Failed: " << xcpt.what() << std::endl;
    }
}

static int* volatile global_null_ptr = NULL;
static int volatile global_zero = 0;
static int volatile global_two = 2;

int main()
{
    CrashGuard::init();

    run_test("No-error-test", []() 
             {
                 int result = 24 / global_two;
             });

    run_test("Mem-fault-test", []()
             {
                 *global_null_ptr = 42;
             });

    run_test("Division-fault-test", []()
             {
                 int result = 24 / global_zero;
             });

    return 0;
}