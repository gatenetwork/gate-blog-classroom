# const-reference R-value binding

This example shows R-value bindings to a const-& caller variable.

## Prerequisites

- CMake v3.0+
- GCC 4+
- MSVC 2005+


## Demo setup

Invoke `build.sh` or `build.bat` to run the build process.

The executable `const_ref_r_value` should construct an object
in a function and returns it.  
The caller takes the result as a `const&` and shows, that the
object survives until the end of scope.
