#include <iostream>

#include <cstdlib>
#include <ctime>

static volatile int global_object_counter = 0;

class RandomNumber
{
private:
    int id;
    int value;
public:
    RandomNumber()
    {
        this->id = ++global_object_counter;
        this->value = rand();
        std::cout << "Construct object #" << this->id << " with value " << this->value << std::endl;
    }

    RandomNumber(RandomNumber const& src)
    :   value(src.value)
    {
        this->id = ++global_object_counter;
        std::cout << "Copy-construct object #" << this->id << " with value " << this->value << std::endl;
    }

    RandomNumber& operator=(RandomNumber const& src)
    {
        this->value = src.value;
        std::cout << "Assign object #" << this->id << " value " << this->value << std::endl;
        return *this;
    }
    ~RandomNumber()
    {
        std::cout << "Destruct object #" << this->id << " with value " << this->value << std::endl;
        this->value = -1; //poison value variable
    }

    int getValue() const
    {
        std::cout << "Return value " << this->value << " of object #" << this->id << std::endl;
        return this->value;
    }
};

static RandomNumber generateRandomNumber()
{
    std::cout << "Checkpoint: enter generate_random_number()" << std::endl;
    srand((unsigned int)time(NULL));
    RandomNumber new_num;
    std::cout << "Checkpoint: leave generate_random_number()" << std::endl;
    return new_num;
}

int main()
{
    std::cout << "Checkpoint: enter main()" << std::endl;
    {
        RandomNumber const& myNumber = generateRandomNumber();
        std::cout << "Checkpoint: generateRandomNumber() returned an object" << std::endl;

        int value = myNumber.getValue();
        std::cout << "Checkpoint: returned value = " << value << std::endl;
    }

    std::cout << "Checkpoint: leave main()" << std::endl;
    return 0;
}
