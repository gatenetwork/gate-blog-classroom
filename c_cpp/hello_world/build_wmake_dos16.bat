@ECHO OFF
SET OLDDIR=%CD%

IF EXIST "C:\WATCOM\owsetenv.bat" (
  CALL "C:\WATCOM\owsetenv.bat"
)

SET BUILD_FOLDER="build\wmake"
del /S /Q %BUILD_FOLDER% >NUL
mkdir %BUILD_FOLDER% >NUL
chdir %BUILD_FOLDER%
REM cmake -G "Watcom WMake" -DCMAKE_SYSTEM_NAME=DOS -DCMAKE_C_COMPILER=C:/WATCOM/binnt64/wcl.exe "-DCMAKE_EXE_LINKER_FLAGS=opt map system dos" "-DCMAKE_MODULE_LINKER_FLAGS=opt map system dos" -DCMAKE_BUILD_TYPE=Release %OLDDIR%
cmake -G "Watcom WMake" -DCMAKE_SYSTEM_NAME=DOS -DCMAKE_SYSTEM_PROCESSOR=I86 -DCMAKE_BUILD_TYPE=Release %OLDDIR%
set CL=/MP
REM wmake -a -p
wmake
chdir /d %OLDDIR%
