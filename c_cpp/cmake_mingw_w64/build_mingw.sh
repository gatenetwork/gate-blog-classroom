#!/bin/sh
export BUILD_DIR=./build
export SOURCE_DIR=.
if [ -d "$BUILD_DIR" ]; then rm -r $BUILD_DIR; fi
mkdir -p $BUILD_DIR
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=./mingw-w64-x86_32.cmake . -B $BUILD_DIR
cmake --build $BUILD_DIR
