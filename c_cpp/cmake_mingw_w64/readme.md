# CMake MinGW-Win32/64 Toolchain for Linux

This example shows how to setup a build process on linux 
using the MinGW tool chain to create Win32 or Win64 binaries.

## Prerequisites

- CMake v3.0+
- MinGW-w64

Most distributions include this toolchain in their default 
software repository.

e.g.: 
`sudo apt install cmake -y`
`sudo apt install gcc-mingw-w64 -y`
`sudo apt install g++-mingw-w64 -y`

## Demo setup

Invoke `build_mingw.sh` to execute `cmake` together with
the delivered tool-chain file `mingw-w64-x86_32.cmake`.
It should create a CMake build folder where the final
executable `hello_world.exe` should appear, after the
build completes successfully.
