#include <stdio.h>
#include <dlfcn.h>

static char const* shared_lib_file = "libshared_lib.so";
static char const* shared_lib_function_name = "shared_lib_function";
static void* shared_lib_handle = NULL;
typedef int(* shared_lib_function_ptr_t)(int);
static shared_lib_function_ptr_t shared_lib_function = NULL;

static int load_shared_lib()
{
	void* sym;

	printf("LOADING shared_lib\n");

	shared_lib_handle = dlopen(shared_lib_file, RTLD_NOW | RTLD_GLOBAL);
	if(NULL == shared_lib_handle)
	{
		printf("%s\n", dlerror());
		return 0;
	}

	sym = dlsym(shared_lib_handle, shared_lib_function_name);
	if(NULL == sym)
	{
		printf("%s\n", dlerror());
		return 0;
	}

	shared_lib_function = (shared_lib_function_ptr_t)sym;

	printf("LOADING shared_lib COMPLETED\n");

	return 1;
}

static void invoke_shared_lib_function()
{
	int result;

	printf("INVOKE shared_lib function\n");
	
	result = shared_lib_function(42);

	printf("FINISHED shared_lib function\n");
}

static void unload_shared_lib()
{
	printf("UNLOADING shared_lib\n");

	dlclose(shared_lib_handle);
	shared_lib_handle = NULL;

	printf("UNLOADING shared_lib COMPLETED\n");
}



int main()
{
	printf("ENTER main()\n");

	if(!load_shared_lib())
	{
		return 1;
	}

	invoke_shared_lib_function();

	unload_shared_lib();

	printf("LEAVE main()\n");

	return 0;
}