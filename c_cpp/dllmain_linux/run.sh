#!/bin/sh

echo "Building binaries"
./build.sh
sleep 1

echo "Run binaries"
LD_LIBRARY_PATH=./build/output ./build/output/dllmain_linux_executable
echo "----------------------------------------"
