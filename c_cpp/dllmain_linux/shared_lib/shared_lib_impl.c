#include "shared_lib.h"

#include <unistd.h>


/* first pair of INIT/UNINIT functions */

void __attribute__((constructor)) shared_lib_init_1()
{
  write(STDOUT_FILENO, "**shared_lib_init_1**\n", 22);
}

void __attribute__((destructor)) shared_lib_uninit_1()
{
  write(STDOUT_FILENO, "**shared_lib_uninit_1**\n", 24);
}


/* second pair of INIT/UNINIT functions */

void __attribute__((constructor)) shared_lib_init_2()
{
  write(STDOUT_FILENO, "**shared_lib_init_2**\n", 22);
}

void __attribute__((destructor)) shared_lib_uninit_2()
{
  write(STDOUT_FILENO, "**shared_lib_uninit_2**\n", 24);
}


/* exported function to be called by main process */

int shared_lib_function(int param)
{
	int ret;
	write(STDOUT_FILENO, "ENTER shared_lib_function()\n", 28);

	ret = param * 2;

	write(STDOUT_FILENO, "LEAVE shared_lib_function()\n", 28);
	return ret;
}
