#!/bin/sh

mkdir -p "build"
rm -rf "build"
cmake -B "build" .
cmake --build "build"
