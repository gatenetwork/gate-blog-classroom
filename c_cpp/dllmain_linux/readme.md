# dllmain for Linux

## Description

This demo shows the behavior of functions flagged with 
`__attribute__((constructor))` and `__attribute__((destructor))` 
in a shared library, compiled with 
[GCC](https://en.wikipedia.org/wiki/GNU_Compiler_Collection) on 
[Linux](https://en.wikipedia.org/wiki/Linux).

## Test setup

We compile a linux shared library `.so` that contains an exported function
and two pairs of `constructor/destructor` functions.
Additionally, we compile an executable that loads the shared library.

All `constructor/destructor` functions do only write their names to `STDOUT`.
They use only POSIX functions (e.g. `write()`) instead of C library functions,
to be independent from C runtime initialization issues.

## Expected behavior

When a shared library is loaded (e.g. with `dlopen`), it automatically
executes all functions with the `constructor` attribute.
Afterwards the exported library function is resolved and executed.
All functions with the counterpart attribute `destructor` are
automatically executed when the library is unloaded (e.g. with `dlclose`).

The abstract execution flow is:

1. Start process and execute `main()`
2. Load library
3. Execute `constructor` functions
4. Library is loaded
5. Load function from loaded library
6. Execute function from loaded library
7. Unload library
8. Execute `destructor` functions
9. Library is unloaded
10. Exit program

## Expected output of test binary

```
ENTER main()
LOADING shared_lib
**shared_lib_init_1**
**shared_lib_init_2**
LOADING shared_lib COMPLETED
INVOKE shared_lib function
ENTER shared_lib_function()
LEAVE shared_lib_function()
FINISHED shared_lib function
UNLOADING shared_lib
**shared_lib_uninit_1**
**shared_lib_uninit_2**
UNLOADING shared_lib COMPLETED
LEAVE main()
```
