.386
.model flat, c

.DATA

asm_func_printf_param DB "Arg# %ld: 0x%lx", 10, 0

.CODE

EXTERN printf:PROC

PUBLIC asm_func_win32

asm_func_win32 PROC
	; --- begin proplog ---
	push ebp		; save stack-frame of caller
	mov ebp, esp	; setup stack-frame of current function
	sub esp, 24		; reserve stack for 6 local variables * 4 bytes
	push ebx		; save other non-volatile registers
	push esi		;
	push edi		;
	; --- end prologue ---

	; now move all parameters in sequence into local stack space (local variables)
	; just to show, how to access them
	mov eax, [ebp + 8 + 0 * 4]	;load arg 1 from caller stack (frame offset +8)
	mov [ebp - 1 * 4], eax		;save arg 1 into local frame stack space (frame offset -4)
	mov eax, [ebp + 8 + 1 * 4]	;load arg 2 from caller stack (frame offset +12)
	mov [ebp - 2 * 4], eax		;save arg 2 into local frame stack space (frame offset -8)
	mov eax, [ebp + 8 + 2 * 4]	;load arg 3 from caller stack (frame offset +16)
	mov [ebp - 3 * 4], eax		;save arg 3 into local frame stack space (frame offset -12)
	mov eax, [ebp + 8 + 3 * 4]	;load arg 4 from caller stack (frame offset +20)
	mov [ebp - 4 * 4], eax		;save arg 4 into local frame stack space (frame offset -16)
	mov eax, [ebp + 8 + 4 * 4]	;load arg 5 from caller stack (frame offset +24)
	mov [ebp - 5 * 4], eax		;save arg 5 into local frame stack space (frame offset -20)
	mov eax, [ebp + 8 + 5 * 4]	;load arg 6 from caller stack (frame offset +28)
	mov [ebp - 6 * 4], eax		;save arg 6 into local frame stack space (frame offset -24)

	; --- begin debug print local variables ---
	; debug-print arg 1
	push [ebp - 1 * 4]					; load arg-1 as printf-value arg
	push 1								; printf-number arg
	push offset asm_func_printf_param	; printf-string
	call printf
	add esp, 3 * 4						; remove 3 32-bit values from stack

	; debug-print arg 2
	push [ebp - 2 * 4]					; load arg-2 as printf-value arg
	push 2								; printf-number arg
	push offset asm_func_printf_param	; printf-string
	call printf
	add esp, 3 * 4						; remove 3 32-bit values from stack

	; debug-print arg 3
	push [ebp - 3 * 4]					; load arg-2 as printf-value arg
	push 3								; printf-number arg
	push offset asm_func_printf_param	; printf-string
	call printf
	add esp, 3 * 4						; remove 3 32-bit values from stack

	; debug-print arg 4
	push [ebp - 4 * 4]					; load arg-2 as printf-value arg
	push 4								; printf-number arg
	push offset asm_func_printf_param	; printf-string
	call printf
	add esp, 3 * 4						; remove 3 32-bit values from stack

	; debug-print arg 5
	push [ebp - 5 * 4]					; load arg-2 as printf-value arg
	push 5								; printf-number arg
	push offset asm_func_printf_param	; printf-string
	call printf
	add esp, 3 * 4						; remove 3 32-bit values from stack

	; debug-print arg 6
	push [ebp - 6 * 4]					; load arg-2 as printf-value arg
	push 6								; printf-number arg
	push offset asm_func_printf_param	; printf-string
	call printf
	add esp, 3 * 4						; remove 3 32-bit values from stack
	; --- end debug print local variables ---

	; --- begin real operations with arguments ---
	; now do some math and sum up all argument values
	mov eax, [ebp - 1 * 4]
	add eax, [ebp - 2 * 4]
	add eax, [ebp - 3 * 4]
	add eax, [ebp - 4 * 4]
	add eax, [ebp - 5 * 4]
	add eax, [ebp - 6 * 4]
	; EAX now holds the result, we can now return ;)
	; --- end real operations with arguments ---

	; --- begin epilogue ---
	pop edi			; restore saved non-volatile registers
	pop esi			;
	pop ebx			;
	mov esp, ebp	; stack cleanup
	pop ebp			; store stack-frame pointer of caller
	; --- end epilogue ---
	ret
asm_func_win32 ENDP

END
