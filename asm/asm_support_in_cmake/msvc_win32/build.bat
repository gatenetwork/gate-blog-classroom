SET BUILD_DIR=%~dp0\build
RMDIR /s /q %BUILD_DIR%
MKDIR %BUILD_DIR%
cmake -A Win32 -B %BUILD_DIR%
cmake --build %BUILD_DIR% --config Release
pause