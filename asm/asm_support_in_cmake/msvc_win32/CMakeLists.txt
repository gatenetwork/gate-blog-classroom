project(msvc_win32_asm C ASM_MASM)

file(GLOB C_SOURCES
  "*.c"
)

file(GLOB ASM_SOURCES
  "*.asm"
)
 
add_executable(${PROJECT_NAME}
  ${C_SOURCES}
  ${ASM_SOURCES}
)

target_link_options(${PROJECT_NAME} PUBLIC 
  "$<$<C_COMPILER_ID:MSVC>:/SAFESEH:NO>"
)