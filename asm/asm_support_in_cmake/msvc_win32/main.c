#include "stdio.h"


extern long __cdecl asm_func_win32(long a1, long a2, long a3, long a4, long a5, long a6);

int main()
{
	static long i32 = 0;
	i32 = asm_func_win32(0x0000000f,
						 0x000000f0,
						 0x00000f00,
						 0x0000f000,
						 0x000f0000,
						 0x00f00000);

	printf("Result 0x%lx\n", i32);

	return 0;
}
