	AREA |.data|,DATA
asm_func_print_param_info_text	DCB "Parameter index", 0x0


	AREA |.text|,CODE,READONLY

	EXTERN print_param_info	; import extern C function "print_param_info"

	EXPORT asm_func

asm_func PROC
	;; prologue begins
	pacibsp						; return address protection
	stp fp, lr, [sp, #-0x30]!	; backup reg-pair(fp,lr) onto stack and subtract 0x30 from stackpointer
		;equal to:
		;	sub sp, sp, #0x30		; sp -= 0x30
		;	str fp, [sp, #0x00]		; *(sp) = fp
		;	str lr, [sp, #0x08]		; *(sp + 0x08) = lr
	stp x19, x20, [sp, #0x10]	;backup reg-pair(x19,x20) on (stack + 0x10)
		;equal to:
		;	str x19, [sp, #0x10]	; *(sp + 0x10) = lr]
		;	str x20, [sp, #0x18]	; *(sp + 0x18) = lr
	str x21, [sp, #0x20]		;backup reg(x21) on (stack + 0x20)
									; *(sp + 0x20) = lr
								; one reg-slot empty due to 16-byte alignment

	mov fp, sp					;frame chain
									; fp = sp

	sub sp, sp, #0x30			; allocate stack space for local variables (6x 64-bit variables)
									; sp -= 0x30
	;; prologue completed

	; input parameters are in x0-r5
	; save all params in local stack variables
	str x0, [sp, #0x00]
	str x1, [sp, #0x08]
	str x2, [sp, #0x10]
	str x3, [sp, #0x18]
	str x4, [sp, #0x20]
	str x5, [sp, #0x28]


	; call: print_param_info(&asm_func_print_param_info_text, 1, stack_stored_x0)
	ldr x0, =asm_func_print_param_info_text
	mov x1, #1
	ldr x2, [sp, #0x00]
	bl print_param_info

	; call: print_param_info(&asm_func_print_param_info_text, 2, stack_stored_x1)
	ldr x0, =asm_func_print_param_info_text
	mov x1, #2
	ldr x2, [sp, #0x08]
	bl print_param_info

	; call: print_param_info(&asm_func_print_param_info_text, 3, stack_stored_x2)
	ldr x0, =asm_func_print_param_info_text
	mov x1, #3
	ldr x2, [sp, #0x10]
	bl print_param_info

	; call: print_param_info(&asm_func_print_param_info_text, 4, stack_stored_x3)
	ldr x0, =asm_func_print_param_info_text
	mov x1, #4
	ldr x2, [sp, #0x18]
	bl print_param_info
	
	; call: print_param_info(&asm_func_print_param_info_text, 5, stack_stored_x4)
	ldr x0, =asm_func_print_param_info_text
	mov x1, #5
	ldr x2, [sp, #0x20]
	bl print_param_info
	
	; call: print_param_info(&asm_func_print_param_info_text, 6, stack_stored_x5)
	ldr x0, =asm_func_print_param_info_text
	mov x1, #6
	ldr x2, [sp, #0x28]
	bl print_param_info

	; load all params from local stack into scratch registers
	ldr x0, [sp, #0x00]
	ldr x1, [sp, #0x08]
	ldr x2, [sp, #0x10]
	ldr x3, [sp, #0x18]
	ldr x4, [sp, #0x20]
	ldr x5, [sp, #0x28]

	; "add" all parameters and return sum to caller
	;
	add x0, x0, x1	; x0 += x1
	add x0, x0, x2	; x0 += x2
	add x0, x0, x3	; x0 += x3
	add x0, x0, x4	; x0 += x4
	add x0, x0, x5	; x0 += x5
	;
	; return value in x0

	;; epilogue begins
	add sp, sp, #0x30				; free stack space for local variables (6x 64-bit)
									;	sp += 0x80
	ldr	x21, [sp, #0x20]			; restore reg(x21) from (stack + 0x20)
									;	x21 = *(sp + 0x20)
	ldp x19, x20, [sp, #0x10];		; restore reg-pair(x19, x20) from (stacl + 0x10)
		;equal to:
		;	ldr x19, [sp, #0x10]	;	x19 = *(sp + 0x10)
		;	ldr x20, [sp, #0x18]	;	x20 = *(sp + 0x18)
	ldp fp, lr, [sp], #0x030		; restore reg-pair(fp, lr) from stack and add
		;equal to:
		;	ldr fp, [sp, #0x00]		;	fp = *(sp)
		;	ldr lr, [sp, #0x08]		;	lr = *(sp + 0x08)
		;	add sp, sp, #0x30		;	sp += 0x30
	autibsp							; validate return address
	;; epilogue ends
	ret								; return
									;	br lr
	ENDP
	END
