#include "stdio.h"

void print_param_info(char const* ptr_text, unsigned index, unsigned long long value)
{
	printf("%s %u: %llx\n", ptr_text, index, value);
}

extern long long asm_func(unsigned short a1, 
						  unsigned long a2, 
						  unsigned long long a3, 
						  unsigned long long a4, 
						  unsigned long long a5, 
						  unsigned long long a6);

int main()
{
	long long i64 = asm_func(0x000000000000ffff,
							 0x00000000eeee0000,
							 0x0000dddd00000000,
							 0x000c000000000000,
							 0x00b0000000000000,
							 0x0a00000000000000);

	printf("Result: 0x%llx\n", i64);

	return 0;
}
