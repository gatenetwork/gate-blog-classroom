.DATA

asm_func_printf_param DB "Arg# %lld: 0x%llx", 10, 0

.CODE

EXTERN printf:PROC

PUBLIC asm_func

asm_func PROC
	; --- proplog begins ---
	
	; move first 4 args into shadow space 
	; (not used and therefore not necessary, just for fun)
	mov [rsp + 8], rcx
	mov [rsp + 16], rdx
	mov [rsp + 24], r8
	mov [rsp + 32], r9

	push rbp	; save all non-volatile variables ...
	push rbx
	push rdi
	push rsi
	push r12
	push r13
	push r14
	push r15	; ... 8 saved registers (64 bytes)
	
	mov rbp, rsp ; init stack-frame pointer
	sub rsp, 48 ; stack for 6 local variables * 8 bytes
	; --- prologue ends ---

	; now move all parameters in sequence into local stack space (local variables)
	; just to show, how to access them
	mov [rbp - 1 * 8], rcx			;save argument 1 into local frame stack space (RBP + 8)
	mov [rbp - 2 * 8], rdx			;save argument 2 into local frame stack space (RBP + 16)
	mov [rbp - 3 * 8], r8			;save argument 3 into local frame stack space (RBP + 24)
	mov [rbp - 4 * 8], r9			;save argument 4 into local frame stack space (RBP + 32)
	mov rax, [rbp + 8 * 8 + 5 * 8]	;read argument 5 from stack
	mov [rbp - 5 * 8], rax			;save argument 5 into local frame stack space (RBP + 40)
	mov rax, [rbp + 8 * 8 + 6 * 8]	;read argument 6 from stack
	mov [rbp - 6 * 8], rax			;save argument 6 into local frame stack space (RBP + 48)

	; debug-print arg 1
	mov rcx, offset asm_func_printf_param	; printf-string
	mov rdx, 1								; printf-number arg
	mov r8, [rbp - 0]						; load arg-1 as printf-value arg
	sub rsp, 32
	call printf
	add rsp, 32

	; debug-print arg 2
	mov rcx, offset asm_func_printf_param	; printf-string
	mov rdx, 2								; printf-number arg
	mov r8, [rbp - 8]						; load arg-2 as printf-value arg
	sub rsp, 32
	call printf
	add rsp, 32

	; debug-print arg 3
	mov rcx, offset asm_func_printf_param	; printf-string
	mov rdx, 3								; printf-number arg
	mov r8, [rbp - 16]						; load arg-3 as printf-value arg
	sub rsp, 32
	call printf
	add rsp, 32

	; debug-print arg 4
	mov rcx, offset asm_func_printf_param	; printf-string
	mov rdx, 4								; printf-number arg
	mov r8, [rbp - 24]						; load arg-4 as printf-value arg
	sub rsp, 32
	call printf
	add rsp, 32

	; debug-print arg 5
	mov rcx, offset asm_func_printf_param	; printf-string
	mov rdx, 5								; printf-number arg
	mov r8, [rbp - 32]						; load arg-5 as printf-value arg
	sub rsp, 32
	call printf
	add rsp, 32

	; debug-print arg 6
	mov rcx, offset asm_func_printf_param	; printf-string
	mov rdx, 6								; printf-number arg
	mov r8, [rbp - 40]						; load arg-6 as printf-value arg
	sub rsp, 32
	call printf
	add rsp, 32

	;no do some math and sum up all arguement values
	mov rax, [rbp - 0]
	add rax, [rbp - 8]
	add rax, [rbp - 16]
	add rax, [rbp - 24]
	add rax, [rbp - 32]
	add rax, [rbp - 40]
	; RAX now holds the result, we can now return ;)

	; epilogue begins
	add rsp, 48 ; release stack for 6 local variables
	pop r15	; pop all non-volatile variables
	pop r14
	pop r13
	pop r12
	pop rsi
	pop rdi
	pop rbx
	pop rbp
	; epilogue ends
	ret
asm_func ENDP

END
