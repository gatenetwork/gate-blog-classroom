#include "stdio.h"


extern long long asm_func(long long a1, long long a2, long long a3, long long a4, long long a5, long long a6);
//extern long long asm_func(long long a1, ...);

int main()
{
	long long i64 = asm_func(0x000000000000ffff,
							 0x00000000ffff0000,
							 0x0000ffff00000000,
							 0x0001000000000000,
							 0x0010000000000000,
							 0x0100000000000000);

	printf("Result 0x%llx\n", i64);

	return 0;
}
