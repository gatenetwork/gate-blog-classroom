#include "stdio.h"


void print_param_info(char const* ptr_text, unsigned index, unsigned long value)
{
	printf("%s %u: %lx\n", ptr_text, index, value);
}

extern unsigned long asm_func(unsigned char a1,
							  unsigned short a2, 
							  unsigned long a3,
							  unsigned long a4);

int main()
{
	unsigned long u32 = asm_func(0x000000ff,
								 0x0000ee00,
								 0x00dd0000,
								 0xcc000000);

	printf("Result: 0x%lx\n", u32);

	return 0;
}
