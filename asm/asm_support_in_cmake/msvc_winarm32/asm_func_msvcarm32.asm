	AREA |.data|,DATA
asm_func_print_param_info_text	DCB "Parameter index", 0x0

	AREA |.text|,CODE,READONLY

	EXTERN print_param_info	; import extern C function "print_param_info"

	EXPORT asm_func				; declare "asm_func" as public function

asm_func PROC
	;; prologue begins
	push { r4-r10, r11, lr }	; save non-volatile registers, Framepointer (r11) and link register
	add r11, sp, #0x1C			; r11 = sp + 0x24 - set stack-chain-link register
								;    we safed r4 to r12, that are 7 registers (7 x 4 bytes = 28 == 0x1C)
								;    the previous link register was just saved before
	sub sp, sp, #0x10			; reserve space for local variables (4 x 32-bit variables)
								; sp -= 0x10
	;; prologue completed

	;
	; Input parameters are in r0, r1, r2, r3
	; store input registers into local stack variables
	str r0, [sp, #0]		; *(sp + 0) = r0	; variable stack_stored_r0
	str r1, [sp, #4]		; *(sp + 4) = r1	; variable stack_stored_r1
	str r2, [sp, #8]		; *(sp + 8) = r2	; variable stack_stored_r2
	str r3, [sp, #12]		; *(sp + 12) = r3	; variable stack_stored_r3

	; call: print_param_info(&asm_func_print_param_info_text, 1, stack_stored_r0)
	ldr r0, =asm_func_print_param_info_text
	mov r1, #1
	ldr r2, [sp, #0]
	bl print_param_info

	; call: print_param_info(&asm_func_print_param_info_text, 2, stack_stored_r1)
	ldr r0, =asm_func_print_param_info_text
	mov r1, #2
	ldr r2, [sp, #4]
	bl print_param_info

	; call: print_param_info(&asm_func_print_param_info_text, 3, stack_stored_r2)
	ldr r0, =asm_func_print_param_info_text
	mov r1, #3
	ldr r2, [sp, #8]
	bl print_param_info

	; call: print_param_info(&asm_func_print_param_info_text, 4, stack_stored_r3)
	ldr r0, =asm_func_print_param_info_text
	mov r1, #4
	ldr r2, [sp, #12]
	bl print_param_info


	; load previously stored registers
	ldr r0, [sp, #0]		; r0 = *(sp + 0)
	ldr r1, [sp, #4]		; r1 = *(sp + 4)
	ldr r2, [sp, #8]		; r2 = *(sp + 8)
	ldr r3, [sp, #12]		; r3 = *(sp + 12)

	; "add" all parameters and return sum to caller
	;
	add r0, r0, r1	; r0 += r1
	add r0, r0, r2	; r0 += r2
	add r0, r0, r3	; r0 += r3
	;
	; return value in r0


	;; epilogue begins
	add sp, sp, #0x10			; free stack space for local variables (4 x 32-bit variables)
								;	sp += 0x10
	pop { r4-r10, r11, pc }		; restore non-volatile registers, old framepointer (r11)
								;	return address is directly restored into to "pc" to jump back
	ENDP
	END
