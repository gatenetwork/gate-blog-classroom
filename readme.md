# opengate.at BLOG CLASSROOM

Welcome to the OpenGATE.at BLOG Classroom repository

## Preamble

This repository contains a set of sample source codes which are published
in parallel to the [OpenGATE BLOG](http://www.opengate.at/blog) and 
[GATE Development project](http://www.opengate.at/main).
The samples are quite small in size and shall help to reproduce test cases
or to inform about features discussed in blog posts.

Some of them are just about documentation of failures.
Others are little programs to show and describe special use cases.


## Prerequisites

- GIT is used as main source control system
  - See: https://git-scm.com/
  - Clone repository with  
    `git clone https://bitbucket.org/gatenetwork/gate-blog-classroom.git`
- Most C/C++ samples are designed to work with CMake, see https://cmake.org/
  - Windows samples should be compilable with 
    Microsoft Visual Studio Build Tools 2017
    https://aka.ms/vs/15/release/vs_buildtools.exe
  - Linux samples should at least be compiled with 
    GCC 7 or newer.
    https://gcc.gnu.org/gcc-7/
- Python scripts are based on Python 3.7 or newer versions
  https://www.python.org/


## License

All sample source codes provided by the `gate-blog-classroom` repository
are published under the 2-clause BSD license:
https://opensource.org/licenses/BSD-2-Clause

------------------------------------------------------------------------------

Copyright 2023 Stefan Meislinger (stefan@opengate.at)

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
